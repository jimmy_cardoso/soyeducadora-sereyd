//
//  DiagnoseModel.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 12/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift

class DiagnoseModel: Object {
    
    var date: String?
    var name: String?
    dynamic var state: Int = 0
    var idDiagnose: String?
    var idGroupDiagnose: String?
    
    let groups = List<GroupModel>()
    let results = LinkingObjects(fromType: ResultModel.self, property: "diagnoses")
    
}
