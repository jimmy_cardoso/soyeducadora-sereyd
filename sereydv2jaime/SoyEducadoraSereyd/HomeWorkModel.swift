//
//  HomeWorkModel.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 02/06/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift

class HomeWorkModel : Object {
	 
	var name: String?
	var descriptionHomeWork: String?
	var date: String?
	var idHomework: String?
	
	let groups = List<GroupModel>()
	
}
