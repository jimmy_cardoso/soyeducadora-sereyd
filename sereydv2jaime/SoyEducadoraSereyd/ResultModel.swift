//
//  Result.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 12/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift

class ResultModel: Object {
    
    dynamic var result: Int = 0
    dynamic var student: StudentModel?
    let diagnoses = List<DiagnoseModel>()
    
}
