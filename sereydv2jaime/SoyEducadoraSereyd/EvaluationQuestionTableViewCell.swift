//
//  EvaluationQuestionTableViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 23/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class EvaluationQuestionTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var typeQuestion: UILabel!
    
    @IBOutlet weak var questionLabel: UITextView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
}
