//
//  Helpers.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 18/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//
import UIKit

extension UITextField {
	
	func roundBorder(){
		
		self.layer.borderColor = ColorHelper.getSecondaryColor(1.0).cgColor
		self.layer.borderWidth = 1.5
		self.layer.masksToBounds = true
		self.layer.cornerRadius = 10
		
	}
	
}

extension UITextView {
    
    func roundBorder(){
        
        self.layer.borderColor = ColorHelper.getSecondaryColor(1.0).cgColor
        self.layer.borderWidth = 1.5
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 10
        
    }
    
}

extension String {
    
    var parseJSONString: Any? {
        
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        if let jsonData = data {
            // Will return an object or nil if JSON decoding fails
            do{
                let json = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers)
                return json
            }catch{
                return nil
            }
        } else {
            // Lossless conversion of the string was not possible
            return nil
        }
    }
}

extension UIView{
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
}
