//
//  AlertTableViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 03/06/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class AlertTableViewCell: UITableViewCell {

	var tapDeleteAction: ((UITableViewCell) -> Void)?
	
	@IBOutlet weak var titleAlertLabel: UILabel!
	@IBOutlet weak var dateAlertLabel: UILabel!
    @IBOutlet weak var descriptionAlert: UITextView!
	
	@IBAction func deleteAlert(_ sender: Any) {
		tapDeleteAction?(self)
	}
	
}
