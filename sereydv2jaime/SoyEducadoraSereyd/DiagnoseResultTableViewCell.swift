
//
//  DiagnoseResultTableViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 30/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class DiagnoseResultTableViewCell: UITableViewCell {

    var tapExportAction: ((UITableViewCell) -> Void)?

    @IBOutlet weak var label0: UILabel!
    @IBOutlet weak var progres0: UIProgressView!
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var progres1: UIProgressView!
    
    @IBOutlet weak var progres2: UIProgressView!
    @IBOutlet weak var label2: UILabel!
    
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var progres3: UIProgressView!
    
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var progres4: UIProgressView!
    
    @IBOutlet weak var progres5: UIProgressView!
    @IBOutlet weak var label5: UILabel!
    
    @IBOutlet weak var dateDiagnose: UILabel!
    
    @IBAction func exportDiagnose(_ sender: Any) {
        
        tapExportAction?(self)
        
    }
    
}
