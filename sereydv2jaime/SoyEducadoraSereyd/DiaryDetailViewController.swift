//
//  DiaryDetailViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 21/11/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class DiaryDetailViewController: UIViewController {
    
    var diaryToPass : Diary!
    
    @IBOutlet weak var diaryDate: UILabel!
    @IBOutlet weak var diaryTitle: UILabel!
    @IBOutlet weak var diaryContentText: UITextView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if diaryToPass != nil {
            
            diaryDate.text = diaryToPass!.value(forKey: "date") as? String
            diaryTitle.text = diaryToPass!.value(forKey: "title") as? String
            diaryContentText.text = diaryToPass!.value(forKey: "subject") as? String
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
