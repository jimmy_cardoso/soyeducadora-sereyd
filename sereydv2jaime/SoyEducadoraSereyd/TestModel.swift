//
//  TestModel.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 25/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation

class TestModel {
    
    var idTest: String?
    var nameTest: String?
    var questionString: String?
    var idQuestion: String?
    var result: Int = -1
    
}
