//
//  PaymentHandlerViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 28/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SwiftyButton
//import Openpay
import CarbonKit

class PaymentHandlerViewController: UIViewController, PayPalPaymentDelegate, NVActivityIndicatorViewable{
    
    @IBOutlet weak var buttonYear: FlatButton!
    @IBOutlet weak var buttonMonth: FlatButton!
    
    //CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
    
    //OPENPAY
//    var openpay: Openpay!
//    var sessionID: String!
//    var tokenID: String!
//    static let MERCHANT_ID = "mbljemtdh8osybgrddpt"//"m0nccdfwzr0pf8mdiuhd"             // Generated in Openpay account registration
//    static let API_KEY = "pk_3ac1af73f8bb48f49260ea17dee82c3d"//"pk_41ece2789ebd46b1aacd2fd62a714c32"  // Generated in Openpay account registration

    //PAYPAL
    /*var environment: String = PayPalEnvironmentSandbox{
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }*/
    
    var environment : String = PayPalEnvironmentProduction{
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    var payPalConfig = PayPalConfiguration() // default
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        PayPalMobile.preconnect(withEnvironment: environment)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //PAYPAL
        // Set up payPalConfig
        payPalConfig.acceptCreditCards = false
        payPalConfig.merchantName = "Sereyd"
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        // Setting the payPalShippingAddressOption property is optional.
        //
        // See PayPalConfiguration.h for details.
        
        payPalConfig.payPalShippingAddressOption = .none;
        // Do any additional setup after loading the view, typically from a nib.
        
//        //OPENPAY
//        openpay = Openpay(withMerchantId: PaymentHandlerViewController.MERCHANT_ID, andApiKey: PaymentHandlerViewController.API_KEY, isProductionMode: true, isDebug: false)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var typePayment: Int!
    
    
    // EMPIEZA OPENPAY
//    @IBAction func doPaymentOpenPay(_ sender: Any) {
//    
//        let button = sender as! FlatButton
//        
//        if button.tag == 3{
//            
//            typePayment = Constants.MONTHLY
//        
//        }else{
//            
//            typePayment = Constants.ANNUALLY
//            
//        }
//        
//
//        openpay.createDeviceSessionId(successFunction: successSessionID, failureFunction: failSessionID)
//    
//    }
//    
//    
//    /**
//     * createDeviceSessionId Success Function
//     */
//    func successSessionID(sessionID: String) {
//
//
//        self.sessionID = sessionID  // Receive in a variable the unique SessionID generated
//        openpay.loadCardForm(in: self, successFunction: successCard, failureFunction: failCard, formTitle: "Openpay")
//
//    }
//
//    /**
//     * createDeviceSessionId Failure Function
//     */
//    func failSessionID(error: NSError) {
//
//        _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
//
//    }
//
//    /**
//     * loadCardForm Success Function
//     */
//    func successCard() {
//
//        openpay.createTokenWithCard(address: nil, successFunction: successToken, failureFunction: failToken)
//
//    }
//
//    /**
//     * loadCardForm Failure Function
//     */
//    func failCard(error: NSError) {
//
//
//        _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo, error en la tarjeta", style: AlertStyle.error)
//
//    }
//
//    /**
//     * createTokenWithCard Success Function
//     */
//    func successToken(token: OPToken) {
//
//
//        let params: [String: Any] = ["type": self.typePayment,"token_id":token.id,"device_session_id":self.sessionID!]
//
//        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
//
//        Alamofire.request("\(Constants.OPENPAY)\(UserController.getUserInDatabase()!.value(forKey: "idUser") as! String)", method: .post, parameters: params)
//            .validate()
//            .response { (dataResponse) in
//
//                self.stopAnimating()
//
//                if PlanningController.getStatePaymentRequest(dataResponse){
//
//                    //let responseString = String(data: dataResponse.data!, encoding: .utf8)
//                    self.requestForAssociatePayment()
//
//                }else{
//
//                    _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal realizando el pago, inténtalo de nuevo", style: AlertStyle.error)
//
//                }
//
//        }
//
//
//    }
//
//    /**
//     * createTokenWithCard Failure Function
//     */
//    func failToken(error: NSError) {
//
//        _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
//
//    }

//--TERMINA OPENPAY
    
    @IBAction func doPayment(_ sender: Any) {
        
        let button = sender as! FlatButton
        var item1 : PayPalItem!
        
        if button.tag == 1{
            
            //MONTH
            typePayment = Constants.MONTHLY
            item1 = PayPalItem(name: "Suscripción SEREYD", withQuantity: 1, withPrice: NSDecimalNumber(string: "6.90"), withCurrency: "USD", withSku: "Hip-0037")
        
        }else if button.tag == 2{
        
            //YEAR
            typePayment = Constants.ANNUALLY
            item1 = PayPalItem(name: "Suscripción SEREYD", withQuantity: 1, withPrice: NSDecimalNumber(string: "69.0"), withCurrency: "USD", withSku: "Hip-0037")
        }
        
        let items = [item1!]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "0.0")
        let tax = NSDecimalNumber(string: "0.0")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: 0, withTax: tax)
        
        let total = subtotal.adding(shipping).adding(tax)
        let payment : PayPalPayment!
        if typePayment == Constants.MONTHLY{
            payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "1pay0 recursos , 5 planeaciones", intent: .sale)
        }else{
            payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "30 recursos , 6 planeaciones", intent: .sale)

        }
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
            
        }
        
    }
    
    // PayPalPaymentDelegate
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            
            self.requestForAssociatePayment()
            
        })
    }
    
    func requestForAssociatePayment(){
     
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        let params = ["id_user":UserController.getUserInDatabase()!.value(forKey: "idUser") as! String,
                      "type":self.typePayment] as [String : Any]
        
        Alamofire.request(Constants.PAYMENT_ENDPOINT, method: .post, parameters: params)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if PlanningController.getStatePaymentRequest(dataResponse){
                    
                    _ = SweetAlert().showAlert("Aviso", subTitle: "Pago creado con éxito", style: AlertStyle.success, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                        if isOtherButton == true {
                            
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                    
                }
                
        }

        
    }

}
