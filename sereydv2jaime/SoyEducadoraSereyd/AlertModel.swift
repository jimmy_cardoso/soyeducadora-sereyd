//
//  AlertModel.swift
//  SoyEducadoraSereyd
//
//  Created by Jimmy Cardoso on 21/06/18.
//  Copyright © 2018 FullStack. All rights reserved.
//

import Foundation
import RealmSwift

class AlertModel : Object {
    
    var name: String?
    var descriptionAlert: String?
    var date: String?
    var time: String?
    var idAlert: String?
    
    let groups = List<GroupModel>()
    
}
