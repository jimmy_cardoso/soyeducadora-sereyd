//
//  ResourceDetailViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 04/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import Alamofire
import PDFReader
import NVActivityIndicatorView
import MessageUI

class ResourceDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable , MFMailComposeViewControllerDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    var items = [ResourceFileModel]()
    var stringIdResourceContainer : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if stringIdResourceContainer != nil{
            requestForResources(stringIdResourceContainer!)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func requestForResources(_ id: String){
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let url = "\(Constants.RESOURCE_ENDPOINT)/\(id)"
        Alamofire.request(url, method: .get)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let resourceListFromServer = ResourceController.getResourceFileFromResponse(dataResponse){
                    
                    self.items = resourceListFromServer
                    self.tableView.reloadData()
                    
                }else{
                    
                    _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "Inténtalo de nuevo más tarde", style: AlertStyle.error)
                    
                }
                
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return items.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ResourceDetailTableViewCell
        cell.nameFile.text = items[indexPath.row].name!
        cell.iconDownload.isHidden = !ResourceController.isResourceFileInDatabase(items[indexPath.row])
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if ResourceController.isResourceFileInDisk(items[indexPath.row]){
            
            if let url = ResourceController.getFileForResource(items[indexPath.row]){
                
                let document = PDFDocument(url: url)!
                showDocument(document, items[indexPath.row])
                
            }else{
                
                _ = SweetAlert().showAlert("Oops", subTitle: "No podemos abrir el archivo. Inténtalo de nuevo más tarde.", style: AlertStyle.warning)
                
            }
            
        }else{
            
            let remotePDFDocumentURLPath = items[indexPath.row].url!
            let remotePDFDocumentURL = URL(string: remotePDFDocumentURLPath)!
            if let doc = document(remotePDFDocumentURL) {
                showDocument(doc,items[indexPath.row])
            }else{
                
                _ = SweetAlert().showAlert("Oops", subTitle: "No podemos abrir el archivo. Inténtalo de nuevo más tarde.", style: AlertStyle.warning)
                
            }
        }
        
    }
    
    // Initializes a document with the remote url of the pdf
    fileprivate func document(_ remoteURL: URL) -> PDFDocument? {
        return PDFDocument(url: remoteURL)
    }
    
    fileprivate func showDocument(_ document: PDFDocument, _ item: ResourceFileModel) {
        
        let image = UIImage(named: "")        
        let controller = PDFViewController.createNew(with: document, title: "", actionButtonImage: image, actionStyle: .customAction({ () in
            
            if ResourceController.isResourceFileInDisk(item){
                
                _ = SweetAlert().showAlert("Aviso", subTitle: "¿Deseas compartir este recurso?", style: AlertStyle.warning, buttonTitle:"No", buttonColor:ColorHelper.getGrayColor(1.0), otherButtonTitle:  "Compartir", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                    if isOtherButton == true {
                        
                    }
                    else {
                        
                        let mailComposeViewController = self.configuredMailComposeViewController(item)
                        if MFMailComposeViewController.canSendMail() {
                            
                            self.present(mailComposeViewController, animated: true, completion: nil)
                            
                        } else {
                            
                            self.showSendMailErrorAlert()
                            
                        }
                    }
                }
           
            }else{
                _ = SweetAlert().showAlert("Aviso", subTitle: "¿Deseas descargar este recurso?", style: AlertStyle.warning, buttonTitle:"No", buttonColor:ColorHelper.getGrayColor(1.0), otherButtonTitle:  "Descargar", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                    if isOtherButton == true {
                        
                    }
                    else {
                        self.downloadResource(item)
                    }
                }
                
            }
           
        }))
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func configuredMailComposeViewController(_ item: ResourceFileModel) -> MFMailComposeViewController {
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        //mailComposerVC.setToRecipients(["nurdin@gmail.com"])
        mailComposerVC.setSubject("Recurso")
        mailComposerVC.setMessageBody("PDF de recurso adjunto", isHTML: false)
        
        let pathFile = ResourceController.getFileForResource(item)
        let fileData = NSData(contentsOf: pathFile!)
        let fileName = ResourceController.getFileNameForResource(item)
        
        mailComposerVC.addAttachmentData(fileData! as Data, mimeType: "application/pdf ", fileName: fileName)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        _ = SweetAlert().showAlert("No pudimos mandar el correo", subTitle: "Tu dispositivo no puede mandar email, configura una cuenta e inténtalo de nuevo.", style: AlertStyle.warning)
        
    }
    
    
    func downloadResource(_ item: ResourceFileModel) -> Void{
        
        let params = ["id_user":UserController.getUserInDatabase()!.value(forKey: "idUser") as! String,
                      "id_resource": item.idResouceFile!]
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)

        Alamofire.request(Constants.RESOURCE_USER_ENPOINT, method: .post, parameters: params).validate()
        .response { (dataResponse) in
            
            self.stopAnimating()
            
            let responseIntCode = ResourceController.asignResponse(dataResponse)
            
            if  responseIntCode == Constants.RES_OK{
                
                self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
                
                let url = URL(string: item.url!)
                let request = NSMutableURLRequest(url: url!)
                
                let task = URLSession.shared.dataTask(with: request as URLRequest){
                    data, response, error in
                    
                    if error != nil{
                        
                        DispatchQueue.main.sync(execute: {
                            
                            self.stopAnimating()
                            _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                            
                        })
                        
                    }else{
                        
                        if let dataData = data{
                            
                            do{
                                
                                var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last as NSURL?
                                let resourceFileName = ResourceController.getFileNameForResource(item)
                                
                                docURL = docURL?.appendingPathComponent(resourceFileName) as NSURL?
                                
                                try dataData.write(to: docURL! as URL)
                                
                                //UPDATE UI
                                DispatchQueue.main.sync(execute: {
                                    
                                    self.stopAnimating()
                                    
                                    ResourceController.addResourceToDatabase(item)
                                    
                                    let resourceLeft = ResourceController.getResourceItemsLeft(dataResponse)
                                    var stringAppend = ""
                                    if resourceLeft != -1 {
                                        stringAppend = "Recursos restantes \(resourceLeft)"
                                    }
                                    _ = SweetAlert().showAlert("Aviso", subTitle: "Recurso descargado \(stringAppend)", style: AlertStyle.success)
                                    
                                })
                                
                            }catch{
                                DispatchQueue.main.sync(execute: {
                                    
                                    self.stopAnimating()
                                    _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                                    
                                })
                                
                            }
                            
                        }else{
                            
                            DispatchQueue.main.sync(execute: {
                                
                                self.stopAnimating()
                                _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                                
                            })
                            
                        }
                        
                    }
                    
                }
                
                task.resume()
                
            }else {
                
                _ = SweetAlert().showAlert("Error", subTitle: "Verifica tu conexión a internet o que cuentes con recursos disponibles para descargar, inténtalo después.", style: AlertStyle.error)
               
            }
            
        }
        
        
        
       
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    
    
}
