//
//  SettingsViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 18/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {

    override func viewDidLoad() {
        
        super.viewDidLoad()
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
    
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 && indexPath.row == 0{
            
            self.performSegue(withIdentifier: "changePasswordSegue", sender: self)
            
        }else if indexPath.section == 1 && indexPath.row == 1 {
            
            SweetAlert().showAlert("Cerrar sesión", subTitle: "¿Quieres cerrar sesión?", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor: ColorHelper.getGrayColor(1.0) , otherButtonTitle:  "Ok", otherButtonColor: ColorHelper.getRedColor(1.0)) { (isOtherButton) -> Void in
                
                if isOtherButton == false {
                    
                    UserController.deleteSessionUser()
                    self.performSegue(withIdentifier: "unwindSession", sender: self)
                    
                }
            }
            
        }
        else if indexPath.section  == 1 && indexPath.row == 2 {
            
           //self.performSegue(withIdentifier: "showPaymentInformation"/*"PaymentVC"*/, sender: self)
            let paymentController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: /*"showPaymentInformation"*/"payment")
            self.navigationController?.pushViewController(paymentController, animated: true)
        }
        else if indexPath.section == 0 && indexPath.row == 0{
            
            openURL(Constants.FAQ_URL)
            
        }
        else if indexPath.section == 0 && indexPath.row == 1{
            
            openURL(Constants.TERMS_URL)
            
        }
        
    }
    
    func openURL(_ urlString: String){
        
        let url = URL(string: urlString)!
        if #available(iOS 10.0, *) {
        
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        
        } else {
            
            UIApplication.shared.openURL(url)
        
        }
    }


}
