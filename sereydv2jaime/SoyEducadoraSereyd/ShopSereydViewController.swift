//
//  ShopSereydViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 21/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ShopSereydViewController: UIViewController, NVActivityIndicatorViewable {

	@IBOutlet weak var webView: UIWebView!
	
	override func viewDidLoad() {
		
		super.viewDidLoad()
		
		// Do any additional setup after loading the view, typically from a nib.
		CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
		
		let url = URL(string: Constants.urlShop)
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        if let unwrappedUrl = url {
            
            let request = URLRequest(url: unwrappedUrl)
            let session = URLSession.shared
            
            let task = session.dataTask(with: request){
                (data,response,error) in
                
                
                DispatchQueue.main.sync {
                    self.stopAnimating()
                }
                
                if error == nil{
                    
                    self.webView.loadRequest(request)
                }
            }
            
            task.resume()
            
        }
    }


}
