//
//  ListDayStartViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 21/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
import Alamofire
import Kingfisher
import NVActivityIndicatorView
import ActionButton
import PDFReader
import MessageUI

class ListDayStartViewController: UIViewController, UITableViewDataSource,UITableViewDelegate, NVActivityIndicatorViewable, MFMailComposeViewControllerDelegate {
    
    var group: GroupModel?
    var students =  [StudentModel]()
    var list = [AssistModel]()
    var actionButton: ActionButton!
    let urlPDFList = "https://app.soyeducadorasereyd.com/group/information/"
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        
        actionButton = ActionButton(attachedToView: self.view, items: [])
        actionButton.action = { button in
            self.listComplete()
        }
        
        actionButton.setImage(UIImage(named: "icon_done"), forState: .normal)
        actionButton.backgroundColor = ColorHelper.getSecondaryColor(1.0)
        
        let buttonAction = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(ListDayStartViewController.actionStartDay(_:)))
        
        navigationItem.rightBarButtonItem = buttonAction
        
        let nameGroupString = "\(group?.value(forKey: "grade") as! String) \(group?.value(forKey: "group") as! String)"
        self.title = "\(DateUtils.getStringDate()) \(nameGroupString)"
        
        students = StudentController.getStudentInDatabaseByGroup(group!)!
        
        if let listAssist = AssistController.getListForBackup(group!) {
            
            list = listAssist
            
        }else{
            
            for student in students{
                
                let assistObject = AssistModel.AssistObject(idStudentAux: (student.value(forKey: "idStudent") as! String))
                list.append(AssistModel(idGroup: (group!.value(forKey: "idGroup") as! String), assistObject: assistObject))
                
            }
            
        }
        
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Cancelar", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ListDayStartViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
    }
    
    func back(sender: UIBarButtonItem) {
        
        SweetAlert().showAlert("Pase de lista incompleto", subTitle: "¿Deseas guardarlo?", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor: ColorHelper.getGrayColor(1.0) , otherButtonTitle:  "Guardar", otherButtonColor: ColorHelper.getRedColor(1.0)) { (isOtherButton) -> Void in
            
            if isOtherButton == false {
                
                AssistController.saveListForBackup(self.list,self.group!)
                
            }
            
            _ = self.navigationController?.popViewController(animated: true)
            
        }
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return students.count
        
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! StudentListTableViewCell
        
        let nameStudent = "\(students[indexPath.row].value(forKey: "lastname") as! String)\n\(students[indexPath.row].value(forKey: "name") as! String)"
        cell.studentName.text = nameStudent
        
        
        let urlImage = students[indexPath.row].value(forKey: "imageUrl") as! String
        let url = URL(string: urlImage)
        KingfisherManager.shared.retrieveImage(with: url!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
            
            if error == nil{
                
                cell.imageView?.image = ImageUtils.resizeImageXY(image: image!,imageV: cell.imageView!, newWidth: 50.0)
                
            }
            
        })
        
        //Aassist
        cell.switchControlAssist.tag = indexPath.row
        cell.switchControlAssist.addTarget(self, action: #selector(ListDayStartViewController.updateAssist(_:)), for: UIControlEvents.valueChanged)
        let bAssist = list[indexPath.row].assistObject.assist == 1 ? true: false
        cell.switchControlAssist.setOn(bAssist, animated: false)
        
        //Conduct
        cell.segmentedControlConduct.selectedSegmentIndex = list[indexPath.row].assistObject.codunct
        cell.segmentedControlConduct.tag = indexPath.row
        cell.segmentedControlConduct.addTarget(self, action: #selector(ListDayStartViewController.updateConduct(_:)), for: UIControlEvents.valueChanged)
        
        return cell
        
    }
    
    func updateAssist(_ sender: UISwitch){
        
        if sender.isOn{
            
            list[sender.tag].assistObject.assist = 1
            
        }else{
            
            list[sender.tag].assistObject.assist = 0
            
        }
        
    }
    
    func updateConduct(_ sender: UISegmentedControl){
        
        list[sender.tag].assistObject.codunct = sender.selectedSegmentIndex
        
    }
    
    
    //Funciones para mostrar el pdf
    // Initializes a document with the remote url of the pdf
    fileprivate func document(_ remoteURL: URL) -> PDFDocument? {
        return PDFDocument(url: remoteURL)
    }
    //Original
//    fileprivate func showDocument(_ document: PDFDocument) {
//
//        let controller = PDFViewController.createNew(with: document)
//        navigationController?.pushViewController(controller, animated: true)
//    }
    
    //Documento con view para compartit
    fileprivate func showDocument(_ document: PDFDocument) {
        
        let image = UIImage(named: "")
        
        let controller = PDFViewController.createNew(with: document, title: "", actionButtonImage: image, actionStyle: .customAction({ () in
            
            _ = SweetAlert().showAlert("Aviso", subTitle: "¿Deseas compartir esta lista?", style: AlertStyle.warning, buttonTitle:"No", buttonColor:ColorHelper.getGrayColor(1.0), otherButtonTitle:  "Compartir", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                    if isOtherButton == true {
                        
                    }
                    else {
                        
                        let mailComposeViewController = self.configuredMailComposeViewController()
                        
                        if MFMailComposeViewController.canSendMail() {
                            
                            self.present(mailComposeViewController, animated: true, completion: nil)
                            
                        } else {
                            
                            self.showSendMailErrorAlert()
                            
                        }
                        
                    }
                }
       
        }))
        
        navigationController?.pushViewController(controller, animated: true)
        
    }
    
    //Fuciones para compartir por correo
    func configuredMailComposeViewController( ) -> MFMailComposeViewController {
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        //mailComposerVC.setToRecipients(["nurdin@gmail.com"])
        mailComposerVC.setSubject("Lista de asistencia")
        mailComposerVC.setMessageBody("Lista de asistencia PDF", isHTML: false)
        
        let pathFile = NSURL(string: "\(self.urlPDFList)\(self.group?.value(forKey: "idGroup") as! String)")
        let fileData = NSData(contentsOf: pathFile! as URL)
        
        mailComposerVC.addAttachmentData(fileData! as Data, mimeType: "application/pdf ", fileName: "Pase de lista")
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        _ = SweetAlert().showAlert("No pudimos mandar el correo", subTitle: "Tu dispositivo no puede mandar email, configura una cuenta e inténtalo de nuevo.", style: AlertStyle.warning)
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    
    func actionStartDay(_ button: UIBarButtonItem){
        
        let alertView = UIAlertController(title: "Opciones", message: "Comienza tu día", preferredStyle: .actionSheet)
        
        let actionAlert = UIAlertAction(title: "Aviso", style: .default, handler: { (alert) in
            
            self.performSegue(withIdentifier: "detailGroupStart", sender: self)
            
        })
        
        let actionCooperation = UIAlertAction(title: "Cooperación", style: .default, handler: { (alert) in
            
            self.performSegue(withIdentifier: "detailGroupStart", sender: self)
            
        })
        
        let actionHomework = UIAlertAction(title: "Tarea", style: .default, handler: { (alert) in
            
            self.performSegue(withIdentifier: "detailGroupStart", sender: self)
            
        })
        
        let actionPlanning = UIAlertAction(title: "Planeaciones", style: .default, handler: { (alert) in
            
            self.performSegue(withIdentifier: "planningStartDay", sender: self)
            
        })
        
        let actionDiary = UIAlertAction(title: "Diario",style: .default, handler:{ (alert) in
            
            self.performSegue(withIdentifier:"diaryViewController",sender:self)
            
        })
        
        //Exportar lista de estudiantes a PDF
        let actionExportList = UIAlertAction(title: "Asistencia PDF",style: .default, handler:{ (alert) in
            //Te lleva a la vista de los pdf
            //PRueba 1 va a una vsta nueva
            //self.performSegue(withIdentifier:"exportStudentList",sender:self)
            //Prueba 2 intenta con documente de PDFReader
            //Datos de PDf para lista
            //"\(Constants.GROUP_ENDPOINT)\(String(describing: group.value(forKey: "idGroup")!))"
            //let nameGroupString = "\(group?.value(forKey: "grade") as! String) \(group?.value(forKey: "group") as! String)
            //"\(https://app.soyeducadorasereyd.com/group/information/)\(String(describing: self.group?.value(forKey: "idGroup")!))"
            //let document = PDFDocument(url: remotePDFDocumentURL)!
            //        if let doc = document(remotePDFDocumentURL) {
            //            showDocument(doc)
            //        }
            //Prueba 2 intenta con documente de PDFReader
            let remotePDFDocumentURLPath = "\(self.urlPDFList)\(self.group?.value(forKey: "idGroup") as! String)"
            let remotePDFDocumentURL = URL(string: remotePDFDocumentURLPath)!
            if let doc = self.document(remotePDFDocumentURL) {
                self.showDocument(doc)
            }
            
        })
        
        let cancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: { (alert) in
            
            alertView.dismiss(animated: true, completion: nil)
            
        })
        
        alertView.addAction(actionAlert)
        alertView.addAction(actionCooperation)
        alertView.addAction(actionHomework)
        alertView.addAction(actionPlanning)
        alertView.addAction(actionDiary)
        alertView.addAction(actionExportList)
        alertView.addAction(cancel)
        
        self.present(alertView, animated: true, completion: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detailGroupStart" {
            
            let crudViewController = segue.destination as! GroupCustomTabViewController
            crudViewController.group = self.group
            
        }
        
    }
    
    func listComplete(){
        
        var isListComplete : Bool = true
        
        for itemModel in list{
            
            if itemModel.assistObject.assist == -1 || itemModel.assistObject.codunct == -1 {
                
                isListComplete = false
                
            }
            
        }
        
        if isListComplete{
            
            let idGroup = group!.value(forKey: "idGroup") as! String
            
            var jsonList = [JSON]()
            
            for itemModel  in list{
                
                let json : JSON = ["id_student":itemModel.assistObject.idStudent,"attendance":itemModel.assistObject.assist,"behavior":itemModel.assistObject.codunct]
                jsonList.append(json)
                
            }
            
            let jsonToSend : JSON = ["id_group":idGroup,"list":jsonList]
            
            let urlRaw = URL(string: Constants.DAY_PLANNING_ENDPOINT)
            var request = URLRequest(url: urlRaw!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let jsonRaw = jsonToSend.rawString()!.parseJSONString
            request.httpBody = try! JSONSerialization.data(withJSONObject: jsonRaw!)
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            
            Alamofire.request(request).validate()
                .responseJSON { (dataResponse) in
                    
                    self.stopAnimating()
                    
                    let isListResponseCorrect = TestController.getResponseForRangedDiagnose(dataResponse)
                    
                    if isListResponseCorrect{
                        
                        _ = SweetAlert().showAlert("Aviso", subTitle: "Pase de lista correcto", style: AlertStyle.success, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                            
                            AssistController.removeList(self.list, self.group!)
                            self.navigationController?.popViewController(animated: true)
                            
                        }
                        
                    }else{
                        
                        _ = SweetAlert().showAlert("Ooops!", subTitle: "Algo salió mal", style: AlertStyle.warning)
                        
                    }
                    
            }
            
        }else{
            
            _ = SweetAlert().showAlert("Ooops!", subTitle: "Finaliza tu pase de lista correctamente!", style: AlertStyle.warning)
            
        }
        
    }
    
    
    /*override func viewWillDisappear(_ animated : Bool) {
        /*super.viewWillDisappear(animated)*/
        
        if self.isMovingFromParentViewController {
            print("JIIJ")
        }
    }*/
    
}
