//
//  EvaluationController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 23/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

class EvaluationController {
    
    class func addEvaluationToDatabase(_ evaluation: Evaluation) -> Void{
        
        let realm = try! Realm()
        
        try! realm.write {
            
            realm.add(evaluation)
            
        }
        
    }
    
    class func getEvaluationResultFromResponse(_ dataResponse: DefaultDataResponse) -> ResultDiagnose? {
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }

        let result = ResultDiagnose()
        
        let level = jsonResponse?["evaluation_name"] as! String
        let name = jsonResponse?["created_at"] as! String
        let percentage = jsonResponse?["evaluation_percent"] as! Int
        
        result.leve = level
        result.name = name
        result.percent = percentage
        
        return result
        
    }
    
    class func addEvaluationListToDatabase(_ list : [Evaluation],_ group : GroupModel) -> Void{
        
        let listInDatabase = getEvaluationForGroup(group)
        var isEvaluationInDatabase = false
        
        for evaluation in list{
            
            for evaluationDB in listInDatabase!{
                
                if evaluationDB.value(forKey: "idGroupEvaluation") as! String == evaluation.value(forKey: "idGroupEvaluation") as! String{
                    isEvaluationInDatabase = true
                }
            }
            
            if !isEvaluationInDatabase{
                
                evaluation.groups.append(group)
                addEvaluationToDatabase(evaluation)
            }
        }
        
    }

    
    class func getIncompleteEvaluation(_ group: GroupModel) -> Evaluation?{
        
        let listInDatabase = getEvaluationForGroup(group)
        
        for evaluation in listInDatabase!{
            if evaluation.value(forKey: "state") as! Int == Constants.INCOMPLETE{
                
                return evaluation
            }
        }
        
        return nil
        
        
    }
    
    class func getAvailableEvaluationForUserGroup(_ dataResponse: DefaultDataResponse) -> [Evaluation]?{
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        let evaluationArray = jsonResponse?["evaluations"] as! NSArray
        var evaluationsList = [Evaluation]()
        
        for evaluation in evaluationArray{
            
            let evaluation = evaluation as! [String:Any]
            let state = Int(evaluation["completed"] as! String)
            let created = evaluation["created_at"] as! String
            let idEvaluation = evaluation["id_evaluation"] as! String
            let idGroupEvaluation = evaluation["id_group_evaluation"] as! String
            
            let eval = Evaluation()
            eval.date = created
            eval.name = "NAME"
            eval.state = state!
            eval.idEvaluation = idEvaluation
            eval.idGroupEvaluation = idGroupEvaluation
            
            evaluationsList.append(eval)
           
            
        }
        
        return evaluationsList
        
    }
    
    
    class func getEvaluationForGroup(_ group: GroupModel) -> [Evaluation]?{
        
        var items: LinkingObjects<Evaluation>!
        items = group.value(forKey: "evaluations") as? LinkingObjects<Evaluation>
        
        var arrayEvaluation = [Evaluation]()
        
        for homework in items{
            
            arrayEvaluation.append(homework)
            
        }
        
        return arrayEvaluation
        
    }
    
    class func incompleteEvaluationForGroup(_ group: GroupModel) -> Bool{
        
        let listInDatabase = getEvaluationForGroup(group)
        
        for evaluation in listInDatabase!{
            if evaluation.value(forKey: "state") as! Int == Constants.INCOMPLETE{
                
                return true
            }
        }
        
        return false
        
    }

    
    class func editEvaluationStateInDatabase(evaluation: Evaluation,state: Int) -> Void{
        
        let realm = try! Realm()
        
        // Delete an object with a transaction
        try! realm.write {
            
            evaluation.setValue(state, forKey: "state")
            
        }
        
    }
    
    
    class func getAvailableEvaluationFromResponse(_ dataResponse: DefaultDataResponse) -> [EvaluationSelection]?{
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        let arrayEvaluation = jsonResponse?["evaluations"] as! NSArray
        var arrayEvaluationSelection = [EvaluationSelection]()
        
        for evaluation in arrayEvaluation{
            
            let objectEvaluation = evaluation as! [String:Any]
            let name = objectEvaluation["name"] as! String
            let id = objectEvaluation["id_evaluation"] as! String
            arrayEvaluationSelection.append(EvaluationSelection(idEvaluation: id, name: name))
            
        }
        
        return arrayEvaluationSelection
        
    }
    
    class func getTestFromResponse(_ dataResponse: DefaultDataResponse) -> [TestModel]?{
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        
        let jsonTest = jsonResponse?["questions"] as! NSArray
        var testList = [TestModel]()
        
        for json in jsonTest{
            
            let objectRawTest = json as! [String:Any]
            let idTest = objectRawTest["id_evaluation"] as! String
            let idQuestion = objectRawTest["id_question_evaluation"] as! String
            let questionString = objectRawTest["question"] as! String
            let testAppend = TestModel()
            testAppend.idTest = idTest
            testAppend.nameTest = ""
            testAppend.idQuestion = idQuestion
            testAppend.questionString = questionString
            
            testList.append(testAppend)
            
        }
        
        return testList
        
        
    }
    
    class func getEvaluationResponseForGroup(_ dataResponse: DefaultDataResponse) -> Evaluation? {
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        let group_evaluation = jsonResponse?["group_evaluation"] as! [String:Any]
        
        let idGroupEvaluation = group_evaluation["id_group_evaluation"] as! String
        let idEvaluation = group_evaluation["id_evaluation"] as! String
        let date = group_evaluation["created_at"] as! String
        let state = group_evaluation["completed"] as! Int
        
        let groupEvaluation = Evaluation()
        groupEvaluation.date = date
        groupEvaluation.state = state
        groupEvaluation.idEvaluation = idEvaluation
        groupEvaluation.idGroupEvaluation = idGroupEvaluation
        return groupEvaluation
        
    }
    
    class func getEvaluationFilePath(_ dataResponse: DefaultDataResponse) -> String? {
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        let string = jsonResponse!["path"] as! String
        return string
        
    }
    
    class func getFileNameForEvaluation(_ evaluation: Evaluation) -> String{
        
        return "\(evaluation.value(forKey: "idEvaluation") as! String)_\((evaluation.value(forKey: "idGroupEvaluation") as! String).replacingOccurrences(of: " ", with: "")).xlsx"
        
    }
    
    class func getEvaluationInDatabase() -> [Evaluation]{
     
        let realm = try! Realm()
        let evaluation = realm.objects(Evaluation.self)
        return Array(evaluation)
    
    }
    
    class func removeFileForEvaluation() -> Void{
        
        let fileManager = FileManager.default
        let evaluations = getEvaluationInDatabase()
        
        for evaluation in evaluations{
            
            if isEvaluationFileInDisk(evaluation){
                let pathFile = getFileForEvaluation(evaluation)
                do{
                    
                    try fileManager.removeItem(at: pathFile!)
                    
                }catch _ as NSError {
                    
                    
                    
                }

            }
            
        }
        
    }
    
    class func isEvaluationFileInDisk(_ evaluation: Evaluation) -> Bool{
        
        do{
            
            //Getting a list of the docs directory
            let docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last) as NSURL?
            let contents = try (FileManager.default.contentsOfDirectory(at: docURL! as URL, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles))
            
            for content in contents{
                
                let stringFileName = content.absoluteString
                let namePlanning = getFileNameForEvaluation(evaluation)
                if stringFileName.contains(namePlanning){
                    return true
                }
                
            }
            
        }catch{
            return false
        }
        
        return false

        
    }
    
    class func getFileForEvaluation(_ evaluation: Evaluation) -> URL?{
        
        do{
            let docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last) as NSURL?
            let contents = try (FileManager.default.contentsOfDirectory(at: docURL! as URL, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles))
            
            for content in contents{
                
                let stringFileName = content.absoluteString
                let nameEvaluation = getFileNameForEvaluation(evaluation)
                if stringFileName.contains(nameEvaluation){
                    return content
                }
                
            }
            
            return nil
            
        }catch{
            return nil
        }
        
    }

    
    
}
