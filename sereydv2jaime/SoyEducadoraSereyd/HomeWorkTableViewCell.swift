//
//  HomeWorkTableViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 02/06/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class HomeWorkTableViewCell: UITableViewCell {

	var tapDeleteAction: ((UITableViewCell) -> Void)?
	var tapEditAction: ((UITableViewCell) -> Void)?

	@IBOutlet weak var dateHomeworkLabel: UILabel!
	@IBOutlet weak var nameHomeWorkLabel: UILabel!
    @IBOutlet weak var descriptionHomeworkTextView: UITextView!
	
	
	@IBAction func editHomework(_ sender: Any) {
		tapEditAction?(self)
	}
	
	@IBAction func deleteHomework(_ sender: Any) {
		tapDeleteAction?(self)
	}
}
