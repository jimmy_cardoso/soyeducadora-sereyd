//
//  PlanningDetailViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 10/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Kingfisher
import PDFReader
import Alamofire
import PDFReader
import NVActivityIndicatorView
import Floaty
import MessageUI


class PlanningDetailViewController: UIViewController, NVActivityIndicatorViewable , MFMailComposeViewControllerDelegate{
    
    @IBOutlet weak var planningImage: UIImageView!
    @IBOutlet weak var namePlanning: UILabel!
    @IBOutlet weak var descriptionPlanning: UITextView!
    
    @IBOutlet weak var premiumIndicator: UIImageView!
    @IBOutlet weak var buttonViewPlanning: UIButton!
    
    var planning: Planning!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        setupViewWithPlanning(planning)
        
    }
    
    func setupViewWithPlanning(_ planningSent: Planning){
        
        let url = URL(string:  planningSent.value(forKey: "imagePlanning") as! String)
        planningImage.kf.setImage(with: url)
        namePlanning.text = (planningSent.value(forKey: "name") as! String)
        descriptionPlanning.text = (planningSent.value(forKey: "descriptionPlanning") as! String)
        
        if PlanningController.isPlanningInDatabase(planning){
            buttonViewPlanning.setTitle("Ver Planeación", for: .normal)
        }
        
        premiumIndicator.isHidden = (planning.value(forKey: "premium") as! Int) == Constants.FREE ? true : false
        
    }
    
    
    @IBAction func downloadPlanning(_ sender: Any) {
        
        if PlanningController.isPlanningInDatabase(planning){
            
            if PlanningController.isPlanningFileInDisk(planning){
                
                _ = SweetAlert().showAlert("Oops!", subTitle: "Ya descargaste esta planeación", style: AlertStyle.warning)
                
            }else{
                
                SweetAlert().showAlert("Aviso", subTitle: "¿Quieres descargar esta planeación a tu dispositivo?", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor: ColorHelper.getRedColor(1.0), otherButtonTitle:  "Si", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                    if isOtherButton == false {
                        
                        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
                        
                        let url = URL(string: self.planning.value(forKey: "urlFile") as! String)
                        let request = NSMutableURLRequest(url: url!)
                        
                        let task = URLSession.shared.dataTask(with: request as URLRequest){
                            data, response, error in
                            
                            if error != nil{
                                
                                DispatchQueue.main.sync(execute: {
                                    
                                    self.stopAnimating()
                                    _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                                    
                                })
                                
                            }else{
                                
                                if let dataData = data{
                                    
                                    
                                    DispatchQueue.main.sync(execute: {
                                        
                                        self.stopAnimating()
                                        var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last as NSURL?
                                        let planningFileName = PlanningController.getFileNameForPlanning(self.planning)
                                        docURL = docURL?.appendingPathComponent(planningFileName) as NSURL?
                                        
                                        //Download
                                        self.downloadPlanningForWordFile(docURL,dataData)
                                        
                                    })
                                    
                                }else{
                                    
                                    DispatchQueue.main.sync(execute: {
                                        
                                        self.stopAnimating()
                                        _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                                        
                                    })
                                    
                                }
                                
                            }
                            
                        }
                        
                        task.resume()
                        
                    }
                }
                
                
            }
            
            
        }else{
            
            _ = SweetAlert().showAlert("Error", subTitle: "Adquiere la planeación para poder descargarla.", style: AlertStyle.error)
            
        }
        
    }
    
    func downloadPlanningForWordFile(_ dataPdf: NSURL!, _ dataDataPDF: Data) -> Void{
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let url = URL(string: self.planning.value(forKey: "urlWord") as! String)
        let request = NSMutableURLRequest(url: url!)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest){
            data, response, error in
            
            if error != nil{
                
                DispatchQueue.main.sync(execute: {
                    
                    self.stopAnimating()
                    _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                    
                })
                
            }else{
                
                if let dataData = data{
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.stopAnimating()
                        do{
                            
                            var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last as NSURL?
                            let planningFileName = PlanningController.getFileNameForPlanningWord(self.planning)
                            
                            docURL = docURL?.appendingPathComponent(planningFileName) as NSURL?
                            
                            try dataData.write(to: docURL! as URL)
                            try dataDataPDF.write(to: dataPdf! as URL)
                            
                            //UPDATE UI
                            _ = SweetAlert().showAlert("Aviso", subTitle: "Planeación descargada", style: AlertStyle.success)
                            
                        }catch{
                            
                            _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                            
                        }
                        
                    })
                    
                }else{
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.stopAnimating()
                        _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                        
                    })
                    
                }
                
            }
            
        }
        
        task.resume()
        
    }
    
    
    @IBAction func startPlanningView(_ sender: Any) {
        
        if PlanningController.isPlanningInDatabase(planning){
            
            if PlanningController.isPlanningFileInDisk(planning) {
                
                if let url = PlanningController.getFileForPlanning(planning){
                    
                    let document = PDFDocument(url: url)!
                    let readerController = PDFViewController.createNew(with: document)
                    navigationController?.pushViewController(readerController, animated: true)
                    
                }
                
            }else{
                
                SweetAlert().showAlert("Aviso", subTitle: "¿La planeación no está en tu dispositivo, la quieres descargar?", style: AlertStyle.warning, buttonTitle:"Ver", buttonColor: ColorHelper.getGrayColor(1.0), otherButtonTitle:  "Descargar", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                    if isOtherButton == true {
                        
                        let remotePDFDocumentURLPath = self.planning.value(forKey: "urlFile") as? String
                        
                        if let remotePDFDocumentURL = URL(string: remotePDFDocumentURLPath!), let document = PDFDocument(url: remotePDFDocumentURL){
                            
                            let readerController = PDFViewController.createNew(with: document)
                            self.navigationController?.pushViewController(readerController, animated: true)
                            
                        }else{
                            
                            _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                            
                        }
                        
                    }
                    else {
                        
                        self.downloadPlanning((Any).self)
                        
                    }
                }
            }
            
        }else{
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            
            let url = "\(Constants.PAYMENT_ENDPOINT)\(UserController.getUserInDatabase()?.value(forKey: "idUser") as! String)"
            
            Alamofire.request(url, method: .get)
                .validate()
                .response(completionHandler: { (dataResponse) in
                    
                    self.stopAnimating()
                    if let paymentObject = PaymentControllerSereyd.getPaymentStatus(dataResponse){
                        
                        if paymentObject.codeError == Constants.ACTIVE_SUSCRIPTION{
                            
                            if paymentObject.planningCount > 0{
                                
                                SweetAlert().showAlert("Descargar", subTitle: "Te quedan \(paymentObject.planningCount!) Planeaciones ¿La quieres descargar?", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor: ColorHelper.getRedColor(1.0), otherButtonTitle:  "Descargar", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                                    if isOtherButton == false {
                                        
                                        self.requestForDownloadPlanning()
                                        
                                    }
                                }
                                
                            }else{
                                
                                //ESPERA TU SIGUIENTE FECHA
                                  _ = SweetAlert().showAlert("Oops!", subTitle: "Espera tu siguiente fecha de corte", style: AlertStyle.warning)
                                
                            }
                            
                        }else if paymentObject.codeError == Constants.TEST_PERIOD{
                            
                            if paymentObject.planningCount > 0{
                                
                                let prem = self.planning.value(forKey: "premium") as! Int
                                
                                if prem == Constants.FREE {
                                    
                                    SweetAlert().showAlert("Descargar", subTitle: "Te quedan \(paymentObject.planningCount!) Planeaciones ¿La quieres descargar?", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor: ColorHelper.getRedColor(1.0), otherButtonTitle:  "Descargar", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                                        if isOtherButton == false {
                                            
                                            self.requestForDownloadPlanning()
                                            
                                        }
                                    }
                                    
                                }else{
                                    
                                   _ = SweetAlert().showAlert("Ops..", subTitle: "Este contenido es PREMIUM, suscríbete para poder acceder a más planeaciones y recursos.", style: AlertStyle.error)
                                    
                                }
                                
                            }else{
                                
                                //SUSCRUBIRTE
                                SweetAlert().showAlert("Aviso", subTitle: "Tu periodo de prueba ha finalizado, vuélvete PREMIUM para disfrutar de más planeaciones y recursos.", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor: ColorHelper.getRedColor(1.0), otherButtonTitle:  "Suscribirte", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                                    if isOtherButton == false {
                                     
                                        self.requestForPayment()
                                        
                                    }
                                }
                                
                            }
                            
                        }else if paymentObject.codeError == Constants.PAYMENT_REQUIRED{
                            
                            //SUSCRIBIRTE
                            SweetAlert().showAlert("Aviso", subTitle: "Tu suscripción ha caducado, vuélvete PREMIUM y disfruta de tus planeaciones y recursos.", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor: ColorHelper.getRedColor(1.0), otherButtonTitle:  "Suscribirte", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                                if isOtherButton == false {
                                    
                                    self.requestForPayment()
                                    
                                }
                            }
                            
                        }else{
                            
                            _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                            
                        }
                        
                    }else{
                        
                        _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                        

                    }
                    
                })
        }
        
    }
    
    func requestForDownloadPlanning() -> Void{
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let params = ["id_user":UserController.getUserInDatabase()?.value(forKey: "idUser") as! String,
                      "id_planification":planning.value(forKey: "idPlanning") as! String]
        
        Alamofire.request(Constants.PLANNING_USER_DOWNLOAD, method: .post, parameters: params)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if PlanningController.asignPlanningToDatabase(dataResponse,self.planning){
                    
                    _ = SweetAlert().showAlert("Aviso", subTitle: "Planeación asignada", style: AlertStyle.success)
                    self.buttonViewPlanning.setTitle("Ver Planeación", for: .normal)
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                    
                }
                
        }
        
    }
    
    func requestForPayment() -> Void {
        
        
        self.performSegue(withIdentifier: "paymentVC", sender: self)
        
        
    }
        
    @objc
    private func dismissController(sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func moreOptionsPlanning(_ sender: Any) {
        
        let alertController = UIAlertController(title: nil, message: "Comparte tu planeación", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
            // ...
        }
        alertController.addAction(cancelAction)
        
        let shareAction = UIAlertAction(title: "Compartir", style: .default) { action in
            
            self.shareByEmailPlanning()
            
        }
        alertController.addAction(shareAction)
        
        
        self.present(alertController, animated: true) {
            // ...
        }
        
    }
    
    @IBAction func previewMethod(_ sender: Any) {
        
        let remotePDFDocumentURLPath = planning.value(forKey: "preview") as? String
        let remotePDFDocumentURL = URL(string: remotePDFDocumentURLPath!)
        if let doc = document(remotePDFDocumentURL!) {
            showDocument(doc)
        }
        
    }
    
    // Initializes a document with the remote url of the pdf
    fileprivate func document(_ remoteURL: URL) -> PDFDocument? {
        return PDFDocument(url: remoteURL)
    }
    
    fileprivate func showDocument(_ document: PDFDocument) {
        
        let controller = PDFViewController.createNew(with: document)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    
    func shareByEmailPlanning() -> Void{
        
        if PlanningController.isPlanningInDatabase(planning) && PlanningController.isPlanningFileInDisk(planning){
            
            let mailComposeViewController = configuredMailComposeViewController()
            
            if MFMailComposeViewController.canSendMail() {
                
                self.present(mailComposeViewController, animated: true, completion: nil)
                
            } else {
                
                self.showSendMailErrorAlert()
                
            }
            
        }else{
            
            _ = SweetAlert().showAlert("Aviso", subTitle: "Adquiere la planeación y descárgala para poder compartirla", style: AlertStyle.warning)
            
            
        }
        
        
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        //mailComposerVC.setToRecipients(["nurdin@gmail.com"])
        mailComposerVC.setSubject("Planeación")
        mailComposerVC.setMessageBody("PDF de planeación adjunto", isHTML: false)
        
        let pathFile = PlanningController.getFileForPlanning(planning)
        let fileData = NSData(contentsOf: pathFile!)
        let fileName = PlanningController.getFileNameForPlanning(planning)
        
        let pathFileWord = PlanningController.getFileWordForPlanning(planning)
        let fileDataWord = NSData(contentsOf: pathFileWord!)
        let fileNameWord = PlanningController.getFileNameForPlanningWord(planning)
        
        mailComposerVC.addAttachmentData(fileDataWord! as Data, mimeType: "application/vnd.openxmlformats-officedocument.wordprocessingml.document", fileName: fileNameWord)
        mailComposerVC.addAttachmentData(fileData! as Data, mimeType: "application/pdf ", fileName: fileName)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        _ = SweetAlert().showAlert("No pudimos mandar el correo", subTitle: "Tu dispositivo no puede mandar email, configura una cuenta e inténtalo de nuevo.", style: AlertStyle.warning)
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    
}
