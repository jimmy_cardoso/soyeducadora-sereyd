//
//  ResultDiagnose.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 30/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation


class ResultDiagnose{
    
    var name:String?
    var leve: String?
    var percent: Int?
    
}
