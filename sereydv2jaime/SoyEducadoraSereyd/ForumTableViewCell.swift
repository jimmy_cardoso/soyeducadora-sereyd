//
//  ForumTableViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 19/12/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import Cosmos
import FaveButton

class ForumTableViewCell: UITableViewCell {

    var tapLikeAction: ((ForumTableViewCell) -> Void)?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var questionLabel: UITextView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var commentCount: UILabel!
    @IBOutlet weak var likeCount: UILabel!
    
    
    @IBAction func likeButton(_ sender: Any) {
        tapLikeAction?(self)
    }
    
}
