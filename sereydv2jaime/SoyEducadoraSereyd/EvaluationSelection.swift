//
//  EvaluationSelection.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 23/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation

class EvaluationSelection {

    var idEvaluation : String?
    var name: String?
    
    init(idEvaluation: String,name: String) {
        
        self.idEvaluation = idEvaluation
        self.name = name
        
    }

}
