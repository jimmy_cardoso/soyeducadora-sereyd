//
//  PaymentControllerSereyd.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 27/09/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import Alamofire

class PaymentControllerSereyd {

    class func getPaymentStatus(_ dataResponse: DefaultDataResponse) -> PaymentObjectSereyd?{
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
                
        let objectPayment = jsonResponse
        let error = objectPayment?["error"] as! Int
        let code = objectPayment?["code"] as! Int
        var annual : Bool?
        if let boolVal = objectPayment?["annual"] as? Bool{
            annual = boolVal
        }
        
        let description = objectPayment?["description"] as! String
        let plannificationCount = objectPayment?["planification_count"] as! Int
        let resourceCount = objectPayment?["resource_count"] as! Int
        
        return PaymentObjectSereyd(error: error, codeError: code, description: description, annual: annual, resourceCount: resourceCount, planningCount: plannificationCount)
        
    }
    
    class func getPaymentObject(_ dataResponse: DefaultDataResponse) -> PaymentInformation? {
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        let code = jsonResponse?["code"] as! Int
        let paymentInfo = PaymentInformation(code: code)
        
        switch code {
            
        case Constants.TEST_PERIOD:
            
            paymentInfo.description = "Te encuentras en período de prueba"
            paymentInfo.initDate = jsonResponse?["init_date"] as? String
            paymentInfo.finishDate = jsonResponse?["finish_date"] as? String
            
            break
            
        case Constants.ACTIVE_SUSCRIPTION:
            
            paymentInfo.description = "Tu suscripción está vigente"
            paymentInfo.initDate = jsonResponse?["init_date"] as? String
            paymentInfo.finishDate = jsonResponse?["finish_date"] as? String
            
            //Info pay
            let jsonObjectPaymentInfo = jsonResponse?["info_pay"] as! [String:Any]
            paymentInfo.cancelled = jsonObjectPaymentInfo["cancelled"] as! Int
            
            if (jsonObjectPaymentInfo["card_number"] as? String) != nil {
                
                paymentInfo.cardNumber = jsonObjectPaymentInfo["card_number"] as? String
                paymentInfo.holderName = jsonObjectPaymentInfo["holder_name"] as? String
                paymentInfo.chargeDate = jsonObjectPaymentInfo["charge_date"] as? String
                paymentInfo.amount = jsonObjectPaymentInfo["amount"] as! Double
                paymentInfo.descriptionCard = jsonObjectPaymentInfo["desc"] as? String
                
            }else if (jsonObjectPaymentInfo["cancelled"] as! Int) == Constants.CANCELLED {
                
                paymentInfo.descriptionCard = "Cancelaste tu suscripción a SEREYD"
                
            }else{
                
                paymentInfo.descriptionCard = "Tu suscripción es con PayPal"
                
            }
            
            break
            
        case Constants.PAYMENT_REQUIRED:
            
            paymentInfo.description = "Tu suscripción ha caducado"
            
            break
            
        default:
            
            return nil
            
        }
        
        
        return paymentInfo
    }
    

}
