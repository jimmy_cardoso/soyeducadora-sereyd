//
//  Answer.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 20/12/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation

class Answer{
    
    var idAnswer: String?
    var answer: String?
    var date: String?
    var image: String?
    var name: String?
    var ranking: Int?
    
    init(id: String,answer: String,date: String,image: String,name: String, ranking: Int) {
        
        self.idAnswer = id
        self.answer = answer
        self.date = date
        self.image = image
        self.name = name
        self.ranking = ranking
        
    }
    
}
