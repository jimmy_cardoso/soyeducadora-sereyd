//
//  PlanningCollectionViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 06/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class PlanningCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var namePlanning: UILabel!
    @IBOutlet weak var textDescription: UITextView!
    
    @IBOutlet weak var fieldPlanning: UILabel!
    @IBOutlet weak var imageIndicatorViewDownload: UIImageView!
    
    @IBOutlet weak var imageIndicatorPremium: UIImageView!
}
