//
//  DiagnoseController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 12/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire

class DiagnoseController {
    
    
    class func addDiagnoseToDatabase(_ diagnose: DiagnoseModel) -> Void{
        
        let realm = try! Realm()
        
        try! realm.write {
            
            realm.add(diagnose)
            
        }
        
    }
    
    class func deleteDiagnoseFromDatabase(_ diagnose: DiagnoseModel) -> Void{
        
        let realm = try! Realm()
        
        // Delete an object with a transaction
        try! realm.write {
            realm.delete(diagnose)
        }
        
        
    }
    
    class func getDiagnoseForGroup(_ group: GroupModel) -> [DiagnoseModel]?{
        
        var items: LinkingObjects<DiagnoseModel>!
        items = group.value(forKey: "diagnoses") as? LinkingObjects<DiagnoseModel>
        
        var arrayDiagnose = [DiagnoseModel]()
        
        for homework in items{
            
            arrayDiagnose.append(homework)
            
        }
        
        return arrayDiagnose
        
    }
    
    class func editDiagnoseStateInDatabase(diagnose: DiagnoseModel,state: Int) -> Void{
        
        let realm = try! Realm()
        
        // Delete an object with a transaction
        try! realm.write {
            
            diagnose.setValue(state, forKey: "state")
            
        }
        
    }
    
    class func incompleteDiagnoseByGroup(_ group: GroupModel) -> Bool{
        
        var incompleteDiagnose = false
        let diagnoses = DiagnoseController.getDiagnoseForGroup(group)
        
        for diagnose in diagnoses! {
            
            if (diagnose.value(forKey: "state") as! Int) == Constants.INCOMPLETE{
            
                incompleteDiagnose = true
                
            }
        }
        
        return incompleteDiagnose
        
    }
    
    class func getAvailableDiagnosticFromResponse(_ dataResponse: DefaultDataResponse) -> [DiagnoseModel]?{
        
        guard dataResponse.error == nil else{
            
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        let diagnoseArray = jsonResponse?["diagnostics"] as! NSArray
        var diagnoseToReturn = [DiagnoseModel]()
        
        for diagnose in diagnoseArray{
            
            let diagnoseRaw = diagnose as! [String:Any]
            let idDiagnostic = diagnoseRaw["id_diagnostic"] as! String
            let name = diagnoseRaw["name"] as! String
            
            let diagnoseToAppend = DiagnoseModel()
            diagnoseToAppend.idDiagnose = idDiagnostic
            diagnoseToAppend.name = name
            diagnoseToReturn.append(diagnoseToAppend)
            
        }
        
        return diagnoseToReturn
        
    }
    
    class func getAvailableDiagnosticForUserGroup(_ dataResponse: DefaultDataResponse) -> [DiagnoseModel]?{
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        let diagnoseArray = jsonResponse?["diagnostics"] as! NSArray
        var diagnoseToReturn = [DiagnoseModel]()
        
        for diagnose in diagnoseArray{
            
            let diagnoseRaw = diagnose as! [String:Any]
            let idDiagnostic = diagnoseRaw["diagnostic"] as! String
            
            let state = Int(diagnoseRaw["completed"] as! String)
            let idGroupDiagnostic = diagnoseRaw["id_group_diagnostic"] as! String
            let date = diagnoseRaw["created_at"] as! String
            
            let diagnoseToAppend = DiagnoseModel()
            diagnoseToAppend.idDiagnose = idDiagnostic
            diagnoseToAppend.idGroupDiagnose = idGroupDiagnostic
            diagnoseToAppend.state = state!
            diagnoseToAppend.date = date
            diagnoseToReturn.append(diagnoseToAppend)
            
        }
        
        return diagnoseToReturn
        
    }
    
    class func getDiagnoseResponseForGroup(_ dataResponse: DefaultDataResponse) -> DiagnoseModel? {
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }

        let group_diagnostic = jsonResponse?["group_diagnostic"] as! [String:Any]
        let idGroupDiagnostic = group_diagnostic["id_group_diagnostic"] as! String
        let idDiagnostic = group_diagnostic["id_diagnostic"] as! String
        let date = group_diagnostic["created_at"] as! String
        let state = group_diagnostic["completed"] as! Int
        
        let groupDiagnostic = DiagnoseModel()
        groupDiagnostic.date = date
        groupDiagnostic.state = state
        groupDiagnostic.idGroupDiagnose = idGroupDiagnostic
        groupDiagnostic.idDiagnose = idDiagnostic
        
        return groupDiagnostic
                
        
    }
    
    class func getDiagnoseResultFromResponse(_ dataResponse: DefaultDataResponse) -> [ResultDiagnose]?{
    
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        let diagnoseList = jsonResponse?["final_diagnostics"] as! NSArray
        var diagnoseResultArray = [ResultDiagnose]()
        
        for diagnose in diagnoseList{
             
            let objectDiagnose = diagnose as! [String:Any]
            let level = objectDiagnose["level"] as! String
            let percent = objectDiagnose["percent"] as! Int
            
            let result = ResultDiagnose()
            result.leve = level
            result.percent = percent
            
            diagnoseResultArray.append(result)
            
        }
        
        return diagnoseResultArray
        
    }
    
    class func addDiagnoseListToDatabase(_ list : [DiagnoseModel],_ group : GroupModel) -> Void{
        
        let listInDatabase = getDiagnoseForGroup(group)
        var isDiagnoseInDatabase = false
        
        for diagnose in list{
            
            for diagnoseDB in listInDatabase!{
                
                if diagnoseDB.value(forKey: "idGroupDiagnose") as! String == diagnose.value(forKey: "idGroupDiagnose") as! String{
                    isDiagnoseInDatabase = true
                }
            }
            
            if !isDiagnoseInDatabase{
                
                diagnose.groups.append(group)
                addDiagnoseToDatabase(diagnose)
            }
        }
        
    }
    
    class func incompleteDiagnoseForGroup(_ group: GroupModel) -> Bool{
        
        let listInDatabase = getDiagnoseForGroup(group)
        
        for diagnose in listInDatabase!{
            if diagnose.value(forKey: "state") as! Int == Constants.INCOMPLETE{
                
                return true
            }
        }
        
        return false
        
    }
    
    class func getIncompleteDiagnose(_ group: GroupModel) -> DiagnoseModel?{
        
        let listInDatabase = getDiagnoseForGroup(group)
        
        for diagnose in listInDatabase!{
            if diagnose.value(forKey: "state") as! Int == Constants.INCOMPLETE{
                
                return diagnose
            }
        }
        
        return nil
        
        
    }
    
    class func getDiagnoseFilePath(_ dataResponse: DefaultDataResponse) -> String? {
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        let string = jsonResponse!["path"] as! String
        return string
        
    }
    
    class func getFileNameForDiagnose(_ diagnose: DiagnoseModel) -> String{
        return "\(diagnose.value(forKey: "idDiagnose") as! String)_\((diagnose.value(forKey: "idGroupDiagnose") as! String).replacingOccurrences(of: " ", with: "")).xlsx"
        
    }
    
    class func getDiagnoseInDatabase() -> [DiagnoseModel]{
        
        let realm = try! Realm()
        let evaluation = realm.objects(DiagnoseModel.self)
        return Array(evaluation)
        
    }
    
    class func removeFileForDiagnose() -> Void{
        
        let fileManager = FileManager.default
        let diagnoses = getDiagnoseInDatabase()
        
        for diagnose in diagnoses{
            
            if isEvaluationFileInDisk(diagnose){
                let pathFile = getFileForDiagnose(diagnose)
                do{
                    
                    try fileManager.removeItem(at: pathFile!)
                    
                }catch _ as NSError {
                    
                    
                    
                }
                
            }
            
        }
        
    }
    
    class func isEvaluationFileInDisk(_ diagnose: DiagnoseModel) -> Bool{
        
        do{
            
            //Getting a list of the docs directory
            let docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last) as NSURL?
            let contents = try (FileManager.default.contentsOfDirectory(at: docURL! as URL, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles))
            
            for content in contents{
                
                let stringFileName = content.absoluteString
                let namePlanning = getFileNameForDiagnose(diagnose)
                if stringFileName.contains(namePlanning){
                    return true
                }
                
            }
            
        }catch{
            return false
        }
        
        return false
        
        
    }

    class func getFileForDiagnose(_ diagnose: DiagnoseModel) -> URL?{
        
        do{
            let docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last) as NSURL?
            let contents = try (FileManager.default.contentsOfDirectory(at: docURL! as URL, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles))
            
            for content in contents{
                
                let stringFileName = content.absoluteString
                let nameEvaluation = getFileNameForDiagnose(diagnose)
                if stringFileName.contains(nameEvaluation){
                    return content
                }
                
            }
            
            return nil
            
        }catch{
            return nil
        }
        
    }

    
    
}
