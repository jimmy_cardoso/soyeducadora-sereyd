//
//  ResourcesListViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 01/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class ResourcesListViewController: UIViewController, UITableViewDelegate,UITableViewDataSource, NVActivityIndicatorViewable {

    @IBOutlet weak var tableView: UITableView!
    
    //Variable de si es premium o no
    var planning: Planning!
    
    var items = [ResourceContainerModel]()
    
    func requestForPayment() -> Void {
      
        //self.performSegue(withIdentifier: "payment", sender: self)
        let paymentController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "payment")
        self.navigationController?.pushViewController(paymentController, animated: true)
     
    }
    
    override func viewDidLoad() {
        
        //Verifica si es premiun o no
        let url = "\(Constants.PAYMENT_ENDPOINT)\(UserController.getUserInDatabase()?.value(forKey: "idUser") as! String)"
        
        Alamofire.request(url, method: .get)
            .validate()
            .response(completionHandler: { (dataResponse) in
            if let paymentObject = PaymentControllerSereyd.getPaymentStatus(dataResponse){
                
                if paymentObject.codeError == Constants.ACTIVE_SUSCRIPTION{
                    
                    if paymentObject.resourceCount > 0{
                        
                        _ = SweetAlert().showAlert("Descargar", subTitle: "Te quedan \(paymentObject.resourceCount!) recursos ¿Quieres descargar alguno?", style: AlertStyle.warning, buttonTitle:"Cerrar", buttonColor: ColorHelper.getRedColor(1.0))
                        
                    }else{
                        
                        //ESPERA TU SIGUIENTE FECHA
                        _ = SweetAlert().showAlert("Oops!", subTitle: "Espera tu siguiente fecha de corte", style: AlertStyle.warning)
                        
                    }
                    
                }else if paymentObject.codeError == Constants.TEST_PERIOD{
                    
                    if paymentObject.resourceCount > 0{
                        
                        let prem = self.planning.value(forKey: "premium") as! Int
                        
                        if prem == Constants.ACTIVE_SUSCRIPTION {
                            
                            SweetAlert().showAlert("Descargar", subTitle: "Te quedan \(paymentObject.resourceCount!) recursos ¿La quieres descargar?", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor: ColorHelper.getRedColor(1.0), otherButtonTitle:  "Descargar", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                                if isOtherButton == false {
                                    
                                    super.viewDidLoad()
                                    
                                }
                            }
                            
                        }else{
                            
                            _ = SweetAlert().showAlert("Ops..", subTitle: "Este contenido es PREMIUM, suscríbete para poder acceder a más planeaciones y recursos.", style: AlertStyle.error)
                            
                        }
                        
                    }else{
                        
                        //SUSCRUBIRTE
                        SweetAlert().showAlert("Aviso", subTitle: "Tu periodo de prueba ha finalizado, vuélvete PREMIUM para disfrutar de más planeaciones y recursos.", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor: ColorHelper.getRedColor(1.0), otherButtonTitle:  "Suscribirte", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                            if isOtherButton == false {
                                
                                self.requestForPayment()
                                
                            }
                        }
                        
                    }
                    
                }else if paymentObject.codeError == Constants.PAYMENT_REQUIRED{
                    
                    //SUSCRIBIRTE
                    SweetAlert().showAlert("Aviso", subTitle: "Tu suscripción ha caducado, vuélvete PREMIUM y disfruta de tus planeaciones y recursos.", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor: ColorHelper.getRedColor(1.0), otherButtonTitle:  "Suscribirte", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                        if isOtherButton == false {
                            
                            self.requestForPayment()
                            
                        }
                    }
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                    
                }
                
            }else{
                
                _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                
                
            }
        })
        
        //super.viewDidLoad()
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        getResourcesForCurrentUser()
        
    }
    
    public func getResourcesForCurrentUser() {
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let url = "\(Constants.RESOURCE_USER_ENPOINT)\(UserController.getUserInDatabase()!.value(forKey: "idUser") as! String)"
        
        Alamofire.request(url, method: .get)
        .validate()
        .response { (dataResponse) in
            
            self.stopAnimating()
            
            if let resourcesListForUser = ResourceController.getResourceFileFromResponse(dataResponse){
                
                ResourceController.saveListResourceToDatabase(resourcesListForUser)
                
            }
        }
    
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return items.count
        
    }
    
    var stringId: String?
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ResourceItemCellTableViewCell
        cell.buttonResource.color = ColorHelper.getRandomColorForResource()
        cell.buttonResource.setTitle((items[indexPath.row].name!), for: .normal)
        cell.tapAction = { (cell) in
            
            self.stringId = self.items[indexPath.row].idResource!
            self.performSegue(withIdentifier: "resourceDetailSegue", sender: self)
            
        }
        
        return cell
        
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //Verifica si es premiun o no
        let url = "\(Constants.PAYMENT_ENDPOINT)\(UserController.getUserInDatabase()?.value(forKey: "idUser") as! String)"
        
        Alamofire.request(url, method: .get)
            .validate()
            .response(completionHandler: { (dataResponse) in
            if let paymentObject = PaymentControllerSereyd.getPaymentStatus(dataResponse){
                
                if paymentObject.codeError == Constants.ACTIVE_SUSCRIPTION{
                    
                    if segue.identifier == "resourceDetailSegue" {
                        
                        let viewController = segue.destination as! ResourceDetailViewController
                        viewController.stringIdResourceContainer = self.stringId
                        
                    }
                }
                else{
                    _ = SweetAlert().showAlert("Ops..", subTitle: "Este contenido es PREMIUM, suscríbete para poder acceder a más planeaciones y recursos.", style: AlertStyle.error)
                }
                
            }
        })
        
    }

}
