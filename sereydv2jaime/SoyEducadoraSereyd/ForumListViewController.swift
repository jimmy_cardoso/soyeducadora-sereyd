//
//  ForumListViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 19/12/17.
//  Copyright © 2017 FullStack. All rights reserved.
//
import Alamofire
import UIKit
import Kingfisher
import NVActivityIndicatorView
import FaveButton


class ForumListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NVActivityIndicatorViewable, FaveButtonDelegate {
    
    
    func faveButton(_ faveButton: FaveButton, didSelected selected: Bool){
//        didSelectQuestionLike=true
        print("FaveButton didSelected")
    }
    
    func faveButtonDotColors(_ faveButton: FaveButton) -> [DotColors]? {
        print("FaveButton faveButtonDotColors")
        return nil
    }
    
    
 

    @IBOutlet weak var tableView: UITableView!
    var listForum = [ForumModel]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        //Alamofire.request(Constants.DISCUSSION_ENDPOINT, method: .get)
        Alamofire.request("\(Constants.DISCUSSION_ENDPOINT_USER)\(UserController.getUserInDatabase()!.value(forKey: "idUser")!)", method: .get)
        .validate()
        .response { (dataResponse) in
            
            self.stopAnimating()
            
            if let arrayList = ForumController.getForumList(dataResponse){
                
                if arrayList.count > 0{
                    
                    self.listForum.removeAll()
                    self.listForum.append(contentsOf: arrayList)
                    self.tableView.reloadData()
                }else{
                    
                    _ = SweetAlert().showAlert("Ooops!", subTitle: "¡No hay resultados!", style: AlertStyle.warning, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                }
                
            }else{
                
                _ = SweetAlert().showAlert("Ooops!", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                
            }
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return listForum.count
        
    }


    var modelSelected : ForumModel?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        modelSelected = self.listForum[indexPath.row]
        self.performSegue(withIdentifier: "detailForumQuestionSegue", sender: self)
        
    }
    
    
    //Aux like button
    var didSelectQuestionLike = false
    var selectedQuestionLike : ForumModel?
    var contadorLike = 0
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{

        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ForumTableViewCell
        
        cell.titleLabel.text = self.listForum[indexPath.row].title!
        cell.questionLabel.text = self.listForum[indexPath.row].question!
        cell.likeCount.text = self.listForum[indexPath.row].likeCount!
        //_ = String(listForum[indexPath.row].answerCount!)
        cell.commentCount.text = self.listForum[indexPath.row].answerCount //.answerCount as String
        cell.likeCount.text = self.listForum[indexPath.row].likeCount!
        
    
//        let urlImage = self.listForum[indexPath.row].image!
//        let url = URL(string: urlImage)
//        KingfisherManager.shared.retrieveImage(with: url!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
//            
//            if error == nil{
//                
//                cell.imageProfile.image = ImageUtils.resizeImage(image: image!, newWidth: 40.0)
//                ImageUtils.circularImage(cell.imageProfile, color: ColorHelper.getSecondaryColor(1.0).cgColor)
//                
//            }
//            
//        })
        
        
        if(self.listForum[indexPath.row].like == 1)
        {
              self.didSelectQuestionLike = true
        }
        cell.tapLikeAction = { (cell) in
            
            
            
                if(self.didSelectQuestionLike == false){
                    print("FaveButton didSelected se dio like")
                    self.contadorLike = self.contadorLike + 1
                    //self.selectedQuestionLike = self.listForum[indexPath.row]
                    //self.selectedQuestionLike?.likeCount = String(self.contadorLike)
                    cell.likeCount.text = String(self.contadorLike)
                    //cell.likeCount.text = self.listForum[indexPath.row].likeCount!
                    self.didSelectQuestionLike = true
                }
                else{
                    print("FaveButton didSelected se quito like")
                    self.contadorLike = self.contadorLike - 1
                    //self.selectedQuestionLike = self.listForum[indexPath.row]
                    //self.selectedQuestionLike?.likeCount = String(self.contadorLike)
                    cell.likeCount.text = String(self.contadorLike)
                    //cell.likeCount.text = self.listForum[indexPath.row].likeCount!
                    self.didSelectQuestionLike = false
                }
            
        }

        return cell
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableViewAutomaticDimension
        
    }

    @IBAction func doQuestion(_ sender: Any) {
        
        self.performSegue(withIdentifier: "doQuestionSegue",sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detailForumQuestionSegue" {
        
            let vc = segue.destination as! ForumDetailViewController
            vc.model = self.modelSelected
            
        }
        
    }
    
    
    
    //Actualiza los likes a la BD
    func requestForUpdateLikeCount(_ forum: ForumModel) -> Void{
        
        let params = ["id_user": forum.idUser!,
                      "id_discussion":forum.idDiscussion!,
                      "ranking":forum.ranking] as [String:Any]
        
        Alamofire.request(Constants.DISCUSSION_RANKING, method: .post, parameters: params)
            .validate()
            .response { (defaultDataResponse) in
                
                self.stopAnimating()
                
                if ForumController.getForumList(defaultDataResponse) != nil {
//                    
//                    ForumController.addLike(forum, idUser: params["id_user"], idDiscussion: params["id_discussion"], like: params["ranking"])
                    print("Se agrego un like a la BD")
                    
                    //CooperationController.addCooperationToDatabase(cooperation)
                    
//                    self.view.endEditing(true)
//                    _ = SweetAlert().showAlert("Cooperación agregada", subTitle: "¡Agregaste una cooperación correctamente!", style: AlertStyle.success, buttonTitle:"Ok", buttonColor: ColorHelper.getGreenColor(1.0)) { (button) -> Void in
//
//                        self.navigationController?.popViewController(animated: true)
//
//                    }
                }else{
                      print("No se agrego el like a la BD")
//                    self.view.endEditing(true)
//                    _ = SweetAlert().showAlert("Error", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                    
                    
                }
        }
    }


    
    
    
//    @IBAction func likeButton(_ sender: Any) {
//         likeCount = ForumController
//
//        if(likeFlag == false){
//            contadorLike = contadorLike + 1
//            ForumTableViewCell.l
//            likeCount.text = self.listForum[indexPath.row].likeCount!
//            likeFlag = true
//        }
//        else{
//            contadorLike = contadorLike - 1
//            likeCount.text = String(contadorLike)
//            likeFlag = false
//        }
//
//    }
    
    
    
}
