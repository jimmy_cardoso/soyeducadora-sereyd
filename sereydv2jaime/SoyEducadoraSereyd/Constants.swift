//
//  Constants.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 20/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import UIKit

class Constants{
	
	static let urlShop = "https://www.soyeducadorasereyd.com/tienda/"
	static let FEMALE = 0
	static let MALE = 1
    static let CANCELLED = 1
	static let FREE = 0
    //URL ENDPOINTS
    
    static let CANCEL_SUSCRIPTION_ENDPOINT = "https://app.soyeducadorasereyd.com/subscription/cancel/"
    static let URL_SERVER = "https://app.soyeducadorasereyd.com"
    static let DAY_PLANNING_ENDPOINT = "\(URL_SERVER)/day_planning"
	static let LOGIN_FACEBOOK_ENDPOINT = "\(URL_SERVER)/login/facebook"
	static let LOGIN_ENDPOINT = "\(URL_SERVER)/login"
    static let PASSWORD_RESTORE_ENDPOINT = "\(URL_SERVER)/password"
    static let PASSWORD_RESTORE_ENDPOINT_CHANGE = "\(URL_SERVER)/password/"
	static let USER_ENDPOINT = "\(URL_SERVER)/user/"
    static let USER_IMAGE_ENDPOINT = "\(URL_SERVER)/user/image/"
	static let GROUP_ENDPOINT = "\(URL_SERVER)/group/"
	static let GROUP_BYUSER_ENDPOINT = "\(URL_SERVER)/group/user/"
	static let STUDENT_ENDPOINT = "\(URL_SERVER)/student/"
    static let STUDENT_ENDPOINT_INFO = "\(URL_SERVER)/student/information/"
	static let HOMEWORK_ENDPOINT = "\(URL_SERVER)/homework/"
    static let NOTICE_ENDPOINT = "\(URL_SERVER)/notice/"
	static let COOPERATION_ENDPOINT = "\(URL_SERVER)/cooperation/"
    //Servicio de alertas
    static let ALERT_ENDPOINT = "\(URL_SERVER)/notice/"
    static let PLANIFICATION_ENDPOINT = "\(URL_SERVER)/planification"
    static let PLANIFICATION_USER_ENDPOINT = "\(URL_SERVER)/planification/user/"
    static let PAYMENT_ENDPOINT = "\(URL_SERVER)/payment/"
    static let PLANNING_USER_DOWNLOAD = "\(URL_SERVER)/planification_user/"
    static let DIAGNOSTIC_ENDPOINT = "\(URL_SERVER)/diagnostic"
    static let EVALUATION_USER = "\(URL_SERVER)/evaluation/user/"
    static let EVALUATION = "\(URL_SERVER)/evaluation/"
    static let TEST_ENDPOINT = "\(URL_SERVER)/test/full/"
    static let TEST_ENDPOINT_EVALUATION = "\(URL_SERVER)/evaluation/"
    static let GROUP_DIAGNOSTIC_ENDPOINT = "\(URL_SERVER)/group_diagnostic"
    static let GROUP_EVALUATION_ENDPOINT = "\(URL_SERVER)/group_evaluation"
    static let GROUP_DIAGNOSTIC_PENDING_ENDPOINT = "\(URL_SERVER)/group_diagnostic/pending/"
    static let TEST_COMPLETE_DIAGNOSE_ENDPOINT = "\(URL_SERVER)/group_diagnostic/completed/"
    static let DIAGNOSTE_COMPLETE_ENDPOINT = "\(URL_SERVER)/group_diagnostic/final/"
    static let EVALUATION_COMPLETE_ENDPOINT = "\(URL_SERVER)/group_evaluation/final/"
    static let TEST_COMPLETE_EVALUATION_ENDPOINT = "\(URL_SERVER)/group_evaluation/completed/"
    static let STUDENTS_PENDING_ENDPOINT = "\(URL_SERVER)/group_diagnostic/"
    static let STUDENTS_PENDING_EVALUATION_ENDPOINT = "\(URL_SERVER)/group_evaluation/"
    static let RESOURCE_ENDPOINT = "\(URL_SERVER)/resource"
    static let RESOURCE_USER_ENPOINT = "\(URL_SERVER)/resource_user/"
    static let QUESTION_USER_ENPOINT = "\(Constants.URL_SERVER)/question_student"
    static let QUESTION_EVALUATION_USER_ENPOINT = "\(URL_SERVER)/question_student/evaluation/"
    static let OPENPAY = "\(URL_SERVER)/openpay/"
    static let GROUP_EVALUATION_PENDING_ENDPOINT = "\(URL_SERVER)/group_evaluation/pending/"
    static let DISCUSSION_ENDPOINT = "\(URL_SERVER)/discussion"
    //Servicio nuevo de foro (Pollo)
    static let DISCUSSION_ENDPOINT_USER = "\(URL_SERVER)/discussion_user/"
    static let DISCUSSION_RANKING = "\(URL_SERVER)/discussion_ranking/"
    //Pollo end
    static let URL_DIARY = "\(URL_SERVER)/diary"
    static let URL_ANSWER = "\(URL_SERVER)/answer"
    static let URL_ANSWER_RANKING = "\(URL_SERVER)/answer_ranking"
    static let URL_DISCUSSION = "\(URL_SERVER)/discussion"
    static let TERMS_URL = "https://www.soyeducadorasereyd.com/app/privacidad.html"
    static let FAQ_URL = "https://www.soyeducadorasereyd.com/app/preguntas.html"
    
    static let COMPLETE = 1
    static let INCOMPLETE = 0
    static let RES_OK = 2
    static let RES_CONNECTION = 0
    static let RES_NO_ITEMS_LEFT = 1
    static let MONTHLY = 0
    static let ANNUALLY = 1
    static let ACTIVE_SUSCRIPTION = 2
    static let TEST_PERIOD = 1
    static let PAYMENT_REQUIRED = 3
	
	class func getSizeForLoadingView(view: UIView) -> CGSize{
		
		return CGSize(width: view.bounds.size.width/4.0, height: view.bounds.size.height/4.0)
		
	}
    
    
	
}
