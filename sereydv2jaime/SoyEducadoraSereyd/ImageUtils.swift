//
//  ImageUtils.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 23/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import UIKit

class ImageUtils{
	
	static func circularImage(_ photoImageView: UIImageView?, color : CGColor)
	{
		photoImageView!.layer.frame = photoImageView!.layer.frame.insetBy(dx: 0, dy: 0)
		photoImageView!.layer.borderColor = color
		photoImageView!.layer.cornerRadius = photoImageView!.layer.frame.width / 2 //photoImageView!.frame.height/2
		photoImageView!.layer.masksToBounds = false
		photoImageView!.clipsToBounds = true
		photoImageView!.layer.borderWidth = 3.0
		photoImageView!.contentMode = UIViewContentMode.scaleAspectFill
		
	}
	
	static func downloadAsincImageData(_ urlImage: String) -> Data {
		let url = URL(string: urlImage)
		let data = try? Data(contentsOf: url!)
		return data!
	}
    
    static func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }

    static func resizeImageXY(image: UIImage, imageV : UIImageView, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: imageV.center.x, y: imageV.center.y, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
