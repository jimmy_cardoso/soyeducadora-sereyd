 //
//  DetailGroupCustomTabViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 01/06/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import CarbonKit


class GroupCustomTabViewController: UIViewController, CarbonTabSwipeNavigationDelegate {
	
	var group : GroupModel?
	
	// MARK: Override methods
	override func viewDidLoad() {
		
		super.viewDidLoad()
		setupView()
		
	}
	 
	func setupView() -> Void{
		
		CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
		
		let items = ["Alumno","Cooperación","Tarea","Aviso y/o Junta"]
		
		let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
		carbonTabSwipeNavigation.setNormalColor(ColorHelper.getWhiteColor(1.0))
		carbonTabSwipeNavigation.setSelectedColor(ColorHelper.getSecondaryColor(1.0))
		carbonTabSwipeNavigation.setIndicatorColor(ColorHelper.getSecondaryColor(1.0))
		carbonTabSwipeNavigation.carbonSegmentedControl?.backgroundColor = ColorHelper.getPrimaryColor(1.0)
		carbonTabSwipeNavigation.carbonTabSwipeScrollView.backgroundColor = ColorHelper.getPrimaryColor(1.0)
		carbonTabSwipeNavigation.insert(intoRootViewController: self)
		
	}
    
    func exportStudent(_ button: UIBarButtonItem)
    {
        
        self.performSegue(withIdentifier: "groupCrudFormSegue", sender: self)
        
    }
	
	func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
		
		switch index {
		case 0:
			
			let viewControllerGroupDetail = self.storyboard?.instantiateViewController(withIdentifier: "studentViewController") as? StudentDetailViewController
			viewControllerGroupDetail?.group = self.group!
			return viewControllerGroupDetail!
			
		case 1:
			
			let viewControllerCooperationDetail = self.storyboard?.instantiateViewController(withIdentifier: "cooperationDetailViewController") as? CooperationDetailViewController
			viewControllerCooperationDetail?.group = self.group!
			return viewControllerCooperationDetail!
			
		case 2:
			
			//homeworkViewController
			let viewControllerHomeworkDetail = self.storyboard?.instantiateViewController(withIdentifier: "homeworkViewController") as? HomeWorkDetailViewController
			viewControllerHomeworkDetail?.group = self.group!
			return viewControllerHomeworkDetail!
			
		case 3:
			
			//alertViewController
			let viewControllerAlertDetail = self.storyboard?.instantiateViewController(withIdentifier: "alertViewController") as? AlertDetailViewController
			viewControllerAlertDetail?.group = self.group!
			return viewControllerAlertDetail!
			
		default:
			
			let viewControllerCooperationDetail = self.storyboard?.instantiateViewController(withIdentifier: "cooperationDetailViewController") as? CooperationDetailViewController
			viewControllerCooperationDetail?.group = self.group!
			return viewControllerCooperationDetail!

		}
		
	}
}
