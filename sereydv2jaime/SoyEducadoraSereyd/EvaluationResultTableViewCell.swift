//
//  EvaluationResultTableViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 06/09/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class EvaluationResultTableViewCell: UITableViewCell {
    
    var tapExportAction: ((UITableViewCell) -> Void)?
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var percentage: UILabel!
    @IBOutlet weak var dateEvaluation: UILabel!
    @IBOutlet weak var progress: UIProgressView!
    
    @IBAction func exportEvaluation(_ sender: Any) {
        
        tapExportAction?(self)
        
    }
    
    
}
