//
//  GroupTableViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 24/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class GroupTableViewCell: UITableViewCell {

	var tapDeleteAction: ((UITableViewCell) -> Void)?
	var tapEditAction: ((UITableViewCell) -> Void)?
	
    @IBOutlet weak var labelPercentageAttendance: UILabel!
	@IBOutlet weak var nameGroupLabel: UILabel!
	@IBOutlet weak var manIndicatorLabel: UILabel!
	@IBOutlet weak var womanIndicatorLabel: UILabel!
	@IBOutlet weak var accountStudentsLabel: UILabel!
	
    @IBOutlet weak var progressPercentageAttendance: UIProgressView!
    @IBOutlet weak var alertLabel: UILabel!
	
	@IBAction func deleteButtonAction(_ sender: Any) {
	
		tapDeleteAction?(self)
		
	}
	
	@IBAction func editButtonAction(_ sender: Any) {
		tapEditAction?(self)
	}
}
