//
//  ResourceController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 04/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

class ResourceController {
    
    class func getResourceListContainerFromResponse(_ dataResponse: DefaultDataResponse) -> [ResourceContainerModel]?{
    
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        let resources = jsonResponse?["resources"] as! NSArray
        var resourcesList = [ResourceContainerModel]()
        
        for resourceRaw in resources{
            
            let resourceItem = resourceRaw as! [String:Any]
            let name = resourceItem["name"] as! String
            let idResource = resourceItem["id_resource"] as! String
            let resourceToAdd = ResourceContainerModel(name: name, idResource: idResource)
            resourcesList.append(resourceToAdd)
            
        }
        
        return resourcesList

    }
    
    class func getResourceFileFromResponse(_ dataResponse: DefaultDataResponse) -> [ResourceFileModel]? {
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }

        let resources = jsonResponse?["resources"] as! NSArray
        var resourcesList = [ResourceFileModel]()
        
        for resourceRaw in resources{
            
            let resourceItem = resourceRaw as! [String:Any]
            let name = resourceItem["name"] as! String
            let idResource = resourceItem["id_resource_file"] as! String
            let url = "\(Constants.URL_SERVER)\(resourceItem["url"] as! String)"
            let resourceToAdd = ResourceFileModel()
            resourceToAdd.idResouceFile = idResource
            resourceToAdd.name = name
            resourceToAdd.url = url
            resourcesList.append(resourceToAdd)
            
        }
        
        return resourcesList
        
    }
    
    class func addResourceToDatabase(_ resource: ResourceFileModel) -> Void{
        
        let realm = try! Realm()
        
        try! realm.write {
            
            realm.add(resource)
            
        }
        
    }
    
    class func isResourceFileInDatabase(_ resource: ResourceFileModel) -> Bool{
        
        let docusInDatabase = getResourceInDatabase()
        for resourceInDB in docusInDatabase{
            if resourceInDB.value(forKey: "idResouceFile") as? String == resource.idResouceFile{
                return true
            }
            
        }
     
        return false
    }
    
    class func isResourceFileInDisk(_ resource: ResourceFileModel) -> Bool{
        
        do{
            
            //Getting a list of the docs directory
            let docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last) as NSURL?
            let contents = try (FileManager.default.contentsOfDirectory(at: docURL! as URL, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles))
            
            for content in contents{
                
                let stringFileName = content.absoluteString
                let nameResource = getFileNameForResource(resource)
                if stringFileName.contains(nameResource){
                    return true
                }
                
            }
            
        }catch{
            return false
        }
        
        return false

        
    }
    
    class func getFileNameForResource(_ resource: ResourceFileModel) -> String{
        
        let name = "\(resource.value(forKey: "idResouceFile") as! String)_\((resource.value(forKey: "name") as! String).replacingOccurrences(of: " ", with: "")).pdf"
        let nameAcc = name.folding(options: .diacriticInsensitive, locale: .current)
        return nameAcc
        
    }
    
    class func getFileForResource(_ resource: ResourceFileModel) -> URL?{
        
        do{
            let docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last) as NSURL?
            let contents = try (FileManager.default.contentsOfDirectory(at: docURL! as URL, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles))
            
            for content in contents{
                
                let stringFileName = content.absoluteString
                let nameResource = getFileNameForResource(resource)
                if stringFileName.contains(nameResource){
                    return content
                }
                
            }
            
            return nil
            
        }catch{
            return nil
        }
        
    }
    
    class func removeFileForResource() -> Void{
        
        
        let fileManager = FileManager.default
        let resources = getResourceInDatabase()
        
        for resource in resources{
            
            if isResourceFileInDisk(resource){
                
                let pathFile = getFileForResource(resource)
                do{
                    
                    try fileManager.removeItem(at: pathFile!)
                    
                }catch _ as NSError {
                    
                    
                }
                
                
            }
            
        }
        
    }
    
    class func getResourceInDatabase() -> [ResourceFileModel]{
        
        let realm = try! Realm()
        let resource = realm.objects(ResourceFileModel.self)
        return Array(resource)
        
    }
    
    class func getResourceItemsLeft(_ dataResponse: DefaultDataResponse)->Int{
        
        guard dataResponse.error == nil else{
            
            return -1
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return -1
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return -1
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return -1
            
        }
        
        
        if let dictJson = jsonResponse?["resource_count"] as? [String:Any]{
            
            let itemsLeft = dictJson["payments"] as! String
            
            return Int(itemsLeft)!
        }
        
        return -1
        
        
    }
    
    class func asignResponse(_ dataResponse: DefaultDataResponse)-> Int{
        
        guard dataResponse.error == nil else{
            
            return Constants.RES_CONNECTION
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return Constants.RES_CONNECTION
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return Constants.RES_CONNECTION
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return Constants.RES_NO_ITEMS_LEFT
            
        }
        
        return Constants.RES_OK

    }
    
    class func saveListResourceToDatabase(_ listFromServer: [ResourceFileModel]) -> Void{
        
        let resourcesInDatabase = getResourceInDatabase()
        var isResourceInDatabase = false
        
        for resource in listFromServer{
            
            for resourceDB in resourcesInDatabase{
                
                if resource.value(forKey: "idResouceFile") as! String == resourceDB.value(forKey: "idResouceFile") as! String{
                    isResourceInDatabase = true
                }
            }
            
            if !isResourceInDatabase{
                
                addResourceToDatabase(resource)
                
            }
        }
    }
    
}
