//
//  File.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 25/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import Alamofire

class TestController{
    

    class func getTestFromResponse(_ dataResponse: DefaultDataResponse) -> [TestModel]?{
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        let jsonTest = jsonResponse?["tests"] as! NSArray
        var testList = [TestModel]()
        
        for json in jsonTest{
        
            let objectRawTest = json as! [String:Any]
            let nameTest = objectRawTest["name"] as! String
            let idTest = objectRawTest["id_test"] as! String
            
            let jsonArrayQuestion = objectRawTest["questions"] as! NSArray
            
            for question in jsonArrayQuestion{
                
                let objectQuestion = question as! [String:Any]
                let questionString = objectQuestion["question"] as! String
                let questionId = objectQuestion["id_question"] as! String
                
                let modelTest = TestModel()
                modelTest.idTest = idTest
                modelTest.nameTest = nameTest
                modelTest.idQuestion = questionId
                modelTest.questionString = questionString
                
                testList.append(modelTest)
                
                
            }
            
            
        }
        
        return testList
        
    }
    
    class func getResponseForRangedDiagnose(_ dataResponse: DataResponse<Any>) -> Bool{
        
        guard dataResponse.error == nil else{
            
            return false
            
        } 
        
        guard let dataResponse = dataResponse.data else{
            
            return false
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return false
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return false
            
        }
        
        return true
    }
    
    class func getResponseForCompletingDiagnose(_ dataResponse: DefaultDataResponse) -> Bool{
        guard dataResponse.error == nil else{
            
            return false
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return false
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return false
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return false
            
        }
        
        return true
    }
    
    class func getStudentListForPendingTest(_ dataResponse: DefaultDataResponse) -> [String]?{
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        let jsonArray = jsonResponse?["students_pending"] as! NSArray
        var stringIDArray = [String]()
        
        for student in jsonArray{
            
            let idStudent = student as! String
            stringIDArray.append(idStudent)
            
        }
        
        return stringIDArray
        
    }
    
    class func getStudentArrayInDatabaseForPendingDiagnose(_ stringId: [String], _ group: GroupModel) -> [StudentModel]{
        
        let studentInDatabase = StudentController.getStudentInDatabaseByGroup(group)
        var studentToReturn = [StudentModel]()
        
        for id in stringId{
            
            for student in studentInDatabase!{
                
                if student.value(forKey: "idStudent") as! String == id{
                    
                    studentToReturn.append(student)
                    
                }
                
            }
            
        }
        
        return studentToReturn
        
        
    }
}
