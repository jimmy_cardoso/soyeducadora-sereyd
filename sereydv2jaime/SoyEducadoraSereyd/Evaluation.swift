//
//  Evaluation.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 28/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift
class Evaluation: Object {
    
    var date: String?
    var name: String?
    dynamic var state: Int = 0
    var idEvaluation: String?
    var idGroupEvaluation: String?
    
    let groups = List<GroupModel>()

}
