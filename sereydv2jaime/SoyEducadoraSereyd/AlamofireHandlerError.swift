//
//  AlamofireHandlerError.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 01/06/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import Alamofire
class AlamofireHandlerError{
	
	class func getErrorMessage(_ dataResponse: DataResponse<String>) -> String{
		
		
		switch dataResponse.response!.statusCode {
			case 500:
				return "Verifica tus datos e inténtalo de nuevo."
			default:
				return "Algo salió mal, inténtalo de nuevo más tarde"
		}
		
	}
	
    class func getErrorUpdateMessage(_ dataResponse: DataResponse<String>) -> String?{
        
        
        guard let dataResponseSign = dataResponse.data else{
            
            
            return nil
            
        }
        
        let jsonResponseCredential = try? JSONSerialization.jsonObject(with: dataResponseSign, options: .allowFragments)
        
        guard (jsonResponseCredential as? [String:Any]) != nil else{
            
            return nil
            
        }
        
        let jsonError = jsonResponseCredential as! [String:Any]
        if let description = jsonError["description"] as? String{
            return description
        }
        
        return nil
        
    }
    
    class func getErrorUpdateMessage(_ dataResponse: DefaultDataResponse) -> String?{
        
        
        guard let dataResponseSign = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponseCredential = try? JSONSerialization.jsonObject(with: dataResponseSign, options: .allowFragments)
        
        guard (jsonResponseCredential as? [String:Any]) != nil else{
            
            return nil
            
        }
        
        let jsonError = jsonResponseCredential as! [String:Any]
        if let description = jsonError["description"] as? String{
            return description
        }
        
        return nil
        
    }

}
