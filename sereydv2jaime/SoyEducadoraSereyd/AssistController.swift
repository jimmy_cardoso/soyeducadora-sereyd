//
//  AssistController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 04/01/18.
//  Copyright © 2018 FullStack. All rights reserved.
//

import Foundation
import RealmSwift

class AssistController {
    
    class func saveListForBackup(_ assistList: [AssistModel], _ group: GroupModel){
        
        _ = group.value(forKey: "idGroup") as! String
        
        let realm = try! Realm()
        
        for _ in assistList{
            
            let idGroup = group.value(forKey: "idGroup") as! String
            let predicate = NSPredicate(format: "idGroup = %@",idGroup)
            let assistList = realm.objects(AssistBU.self).filter(predicate)
            
            try! realm.write{
                print("REMOVE")
                realm.delete(assistList)
            }
            
        }
        
        for assist in assistList {
            
            let assistBU = AssistBU()
            assistBU.idStudent = assist.assistObject.idStudent!
            assistBU.idGroup = assist.idGroup!
            assistBU.conduct = assist.assistObject.codunct
            assistBU.assistance = assist.assistObject.assist
            saveAssistBU(assist: assistBU)
            
            
        }
        
    }
    
    class func removeList(_ assistList: [AssistModel], _ group: GroupModel){
    
        let realm = try! Realm()
        
        for _ in assistList{
            
            let idGroup = group.value(forKey: "idGroup") as! String
            let predicate = NSPredicate(format: "idGroup = %@",idGroup)
            let assistList = realm.objects(AssistBU.self).filter(predicate)
            
            try! realm.write{
                realm.delete(assistList)
            }
            
        }
    
    }
    
    class func getListForBackup(_ group: GroupModel) -> [AssistModel]? {
    
        let idGroup = group.value(forKey: "idGroup") as! String
        let realm = try! Realm()
        let list = realm.objects(AssistBU.self).filter("idGroup = '\(idGroup)'")
        
        if list.count > 0 {
            
            var assistModels = [AssistModel]()
            
            for assistBackUp in list {
                
                let idStudent = assistBackUp.value(forKey: "idStudent") as! String
                
                if let student = StudentController.getStudentById(idStudent){
                    
                    let idStudentFromBack = student.value(forKey: "idStudent") as! String
                    let assist = assistBackUp.value(forKey: "assistance") as! Int
                    let conduct = assistBackUp.value(forKey: "conduct") as! Int
                    
                    let assistObject = AssistModel.AssistObject(idStudentAux: idStudentFromBack)
                    assistObject.assist = assist
                    assistObject.codunct = conduct
                    let assistModel = AssistModel(idGroup: idGroup, assistObject: assistObject)
                    assistModels.append(assistModel)
                    
                }
                
            }
            
            
            if assistModels.count < StudentController.getStudentInDatabaseByGroup(group)!.count {
                
                var index = assistModels.count
                let students = StudentController.getStudentInDatabaseByGroup(group)
                
                while index < StudentController.getStudentInDatabaseByGroup(group)!.count {
                    
                    let assistObject = AssistModel.AssistObject(idStudentAux: (students?[index].value(forKey: "idStudent") as! String))
                    assistModels.append(AssistModel(idGroup: (group.value(forKey: "idGroup") as! String), assistObject: assistObject))
                    
                    index = index + 1
                    
                }
                
            }
            
            return assistModels
            
        }else{
            
            return nil
        }
        
        
    }
    
    class func saveAssistBU(assist: AssistBU) -> Void{
        
        let realm = try! Realm()
        
        try! realm.write {
            
            realm.add(assist)
            print("SAVE")
        }
        
    }
    
}
