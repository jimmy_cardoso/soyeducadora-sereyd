//
//  AlertDetailViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 03/06/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import ActionButton
import RealmSwift
import Alamofire
import UserNotifications
import NVActivityIndicatorView

class AlertDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource/*, NVActivityIndicatorViewable  */{


    //var actionButton: ActionButton!
    @IBOutlet weak var tableView: UITableView!
    var group: GroupModel?
    var alerts: [AlertModel] = [AlertModel]()
    var actionButton: ActionButton!
    
    //Configuracion local temporal
    // Properties
    var notificationsArray = [UNNotificationRequest]()
    let dateFormatter = DateFormatter()
    let locale = Locale.current
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupView()
        
    }
    
    func setupView() -> Void{
        
        actionButton = ActionButton(attachedToView: self.view, items: [])
        actionButton.action = { button in
            self.performSegue(withIdentifier: "crudAlertSegue", sender: self)
        }
        
        actionButton.setTitle("+", forState: UIControlState())
        actionButton.backgroundColor = ColorHelper.getSecondaryColor(1.0)
        
        //TABLE VIEW
        dateFormatter.locale = Locale.current
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "crudAlertSegue" {
            
            let destinatioViewController = segue.destination as! AlertCrudViewController
            destinatioViewController.group = self.group
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(true)
        fetchReminders()
        
    }
    
    public func fetchReminders() -> Void{
        
        //FETCH REMINDERS
        
        let center = UNUserNotificationCenter.current()
        notificationsArray = [UNNotificationRequest]()
        let idGroupInDatabase = group!.value(forKey: "idGroup") as! String
        
        center.getPendingNotificationRequests { (notifications) in
            
            for item in notifications{
                
                let reminder = item.identifier.components(separatedBy: "_")
                let idGroup = reminder[0]
                
                if idGroup == idGroupInDatabase {
                    self.notificationsArray.append(item)
                }
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return notificationsArray.count
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AlertTableViewCell
        let notification = notificationsArray[indexPath.row]
        
        cell.titleAlertLabel.text = notification.content.title
        cell.descriptionAlert.text = notification.content.body
        
        let trigger = notification.trigger as! UNCalendarNotificationTrigger
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.dateFormat = "dd-MM-YYYY HH:MM:SS"
        dateFormatter.timeStyle = .medium
        
        var dateAux = trigger.dateComponents
        dateAux.year = -16
        cell.dateAlertLabel.text = "\(dateFormatter.string(from: dateAux.date!))"
        
        
        cell.tapDeleteAction = { (cell) in
            
            _ = SweetAlert().showAlert("¿Eliminar esta alerta?", subTitle: "", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor:ColorHelper.getGreenColor(1.0), otherButtonTitle:  "Eliminar",otherButtonColor: ColorHelper.getRedColor(1.0)) { (isOtherButton) -> Void in
                if isOtherButton != true {
                    
                    
                    let identifier = self.notificationsArray[indexPath.row].identifier
                    UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [identifier])
                    self.fetchReminders()
                    _ = SweetAlert().showAlert("Alerta eliminada!", subTitle: "Has eliminado correctamente una tarea", style: AlertStyle.success)
                    
                }
            }
            
            
        }
        
        return cell
        
    }
    
}//<-Eliminar despues
//    // Properties
//    //var notificationsArray = [UNNotificationRequest]()
//    let dateFormatter = DateFormatter()
//    //let locale = Locale.current
//
//    override func viewDidLoad() {
//
//        super.viewDidLoad()
//        setupView()
//
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(true)//Aver :v
//
//        print(requestForAlertsInServer(group!))
//        //requestForAlert()
//
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        tableView.estimatedRowHeight = 100
//        tableView.rowHeight = UITableViewAutomaticDimension
//    }
//
//    func requestForAlertsInServer(_ group: GroupModel) -> Void{
//
//        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
//
//        let url = "\(Constants.ALERT_ENDPOINT)"//"\(String(describing: group.value(forKey: "idGroup")!))"
//        Alamofire.request(url, method: .get)
//            .validate().response { (defaultDataResponse) in
//
//                self.stopAnimating()
//
//                if let alerts = AlertController.getAlertListFromResponse(defaultDataResponse){
//                    AlertController.saveListUnrepeatedAlertInList(alerts,self.group!)
//                    self.alerts = AlertController.getAlertInDatabase(self.group!)
//                    self.tableView.reloadData()
//                }else{
//
//                    _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "Inténtalo de nuevo más tarde", style: AlertStyle.error)
//
//                }
//        }
//
//
//    }
//
////    func requestForAlert() -> Void{
////
////        //GET IMAGE AND SAVE OBJECT
////        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
////
////        // TODO:- Corregir por el servicio adecuado
////        Alamofire.request("\(Constants.ALERT_ENDPOINT)\(String(describing: group!.value(forKey: "idGroup")!))", method: .get)
////            .validate()
////            .response { (dataResponse) in
////                print(dataResponse)
////                self.stopAnimating()
////
////                if let alerts = AlertController.getAlertListFromResponse(dataResponse) {
////
////                    AlertController.saveListUnrepeatedAlertInList(alerts,self.group!)
////                    self.alerts = AlertController.getAlertsForGroup(self.group!)!
////                    self.tableView.reloadData()
////
////                }else{
////
////                    _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "Inténtalo de nuevo más tarde", style: AlertStyle.error)
////
////                }
////
////
////        }
////    }
////
//
//    func setupView() -> Void{
//
//        actionButton = ActionButton(attachedToView: self.view, items: [])
//        actionButton.action = { button in
//            self.performSegue(withIdentifier: "crudAlertSegue", sender: self)
//        }
//
//        actionButton.setTitle("+", forState: UIControlState())
//        actionButton.backgroundColor = ColorHelper.getSecondaryColor(1.0)
//        // TODO:- Corregir
//        print(group!)
//        //alerts = AlertController.getAlertsForGroup(group!)!
//        // TODO:- bug
//        alerts = AlertController.getAlertInDatabase(group!)
//        print(alerts)
//        //TABLE VIEW
////        dateFormatter.locale = Locale.current
////        dateFormatter.dateStyle = .medium
////        dateFormatter.timeStyle = .short
//        
//    }
//
//
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//        if segue.identifier == "crudAlertSegue" {
//
//            let destinatioViewController = segue.destination as! AlertCrudViewController
//            destinatioViewController.group = self.group
//
//        }
//
//    }
//
//    /*override func viewDidAppear(_ animated: Bool) {
//
//        super.viewDidAppear(true)
//        //fetchReminders()
//
//    }
//
//    public func fetchReminders() -> Void{
//
//        //FETCH REMINDERS
//
//        let center = UNUserNotificationCenter.current()
//        notificationsArray = [UNNotificationRequest]()
//        let idGroupInDatabase = group!.value(forKey: "idGroup") as! String
//
//        center.getPendingNotificationRequests { (notifications) in
//
//            for item in notifications{
//
//                let reminder = item.identifier.components(separatedBy: "_")
//                let idGroup = reminder[0]
//
//                if idGroup == idGroupInDatabase {
//                    self.notificationsArray.append(item)
//                }
//
//            }
//
//            DispatchQueue.main.async {
//                self.tableView.reloadData()
//            }
//
//        }
//
//    }*/
//
//    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
//        return alerts.count
//        //return notificationsArray.count
//
//    }
//
//    var didSelectAlertToEdit = false
//    var selectedAlertToEdit: AlertModel?
//
//    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->  UITableViewCell{
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AlertTableViewCell
//        //let notification = notificationsArray[indexPath.row]
//
//        cell.titleAlertLabel.text = alerts[indexPath.row].value(forKey: "name") as? String
//        cell.dateAlertLabel.text = alerts[indexPath.row].value(forKey: "date") as? String
//        cell.descriptionAlert.text = alerts[indexPath.row].value(forKey: "descriptionAlert") as? String
//
//        cell.tapDeleteAction = { (cell) in
//
//            _ = SweetAlert().showAlert("¿Eliminar esta alerta?", subTitle: "", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor:ColorHelper.getGreenColor(1.0), otherButtonTitle:  "Eliminar",otherButtonColor: ColorHelper.getRedColor(1.0)) { (isOtherButton) -> Void in
//                if isOtherButton != true {
//
//                    self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
//                    let url = "\(Constants.NOTICE_ENDPOINT)\(String(describing: self.alerts[indexPath.row].value(forKey: "idNotice")!))"
//
//                    Alamofire.request(url, method: .delete)
//                        .validate()
//                        .response(completionHandler: { (dataResponse) in
//
//                            self.stopAnimating()
//
//                            if AlertController.deleteResponseFromServer(dataResponse){
//
//                                AlertController.deleteAlertInDatabase(alert: self.alerts[indexPath.row])
//
//                                self.alerts = AlertController.getAlertInDatabase(self.group!)
//                                self.tableView.reloadData()
//
//                                _ = SweetAlert().showAlert("Cooperación eliminada!", subTitle: "Has eliminado correctamente una cooperación", style: AlertStyle.success)
//
////                            let boolResponse = AlertController.getResponseFromDeleteAlert(dataResponse)
////                            if boolResponse{
////
////                                AlertController.deleteAlertInDatabase(alert: self.alerts[indexPath.row])
////                                self.alerts = AlertController.getAlertsForGroup(self.group!)!
////
////                                self.tableView.reloadData()
////                                _ = SweetAlert().showAlert("Alerta eliminada!", subTitle: "Has eliminado correctamente una alerta", style: AlertStyle.success)
//                            }else{
//
//                            }
//                        })
//                }
//            }
//        }
//
////        cell.tapEditAction = { (cell) in
////
////            self.selectedAlertToEdit = self.alerts[indexPath.row]
////            self.didSelectAlertToEdit = true
////            self.performSegue(withIdentifier: "crudHomeworkSegue", sender: self)
////
////        }
//
////        cell.titleAlertLabel.text = notification.content.title
////        cell.descriptionAlert.text = notification.content.body
//
////        let trigger = notification.trigger as! UNCalendarNotificationTrigger
//
//
//
//        /*let dateFormatter = DateFormatter()
//        dateFormatter.dateStyle = .short
//        dateFormatter.dateFormat = "dd-MM-YYYY HH:MM:SS"
//        dateFormatter.timeStyle = .medium
//
//        var dateAux = trigger.dateComponents
//        dateAux.year = -16
//        cell.dateAlertLabel.text = "\(dateFormatter.string(from: dateAux.date!))"*/
//
//
////        cell.tapDeleteAction = { (cell) in
////
////            _ = SweetAlert().showAlert("¿Eliminar esta alerta?", subTitle: "", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor:ColorHelper.getGreenColor(1.0), otherButtonTitle:  "Eliminar",otherButtonColor: ColorHelper.getRedColor(1.0)) { (isOtherButton) -> Void in
////                if isOtherButton != true {
////
////
////                    let identifier = self.notificationsArray[indexPath.row].identifier
////                    UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [identifier])
////                    self.fetchReminders()
////                    _ = SweetAlert().showAlert("Alerta eliminada!", subTitle: "Has eliminado correctamente una tarea", style: AlertStyle.success)
////
////                }
////            }
////
////
////        }
//
//        return cell
//
//    }
//
//
//    // When returning from AddReminderViewController
//    @IBAction func unwindToReminderList(_ sender: UIStoryboardSegue) {
//        /*if let sourceViewController = sender.source as? AlertCrudViewController, let reminder = sourceViewController.reminder {
//
//        }*/
//    }
//
//
//}
