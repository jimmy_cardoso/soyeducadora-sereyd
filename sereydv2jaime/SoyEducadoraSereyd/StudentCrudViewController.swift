//
//  StudentCrudViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 29/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import SwiftValidator
import RealmSwift
import Alamofire
import NVActivityIndicatorView
import Kingfisher

class StudentCrudViewController: UIViewController, ValidationDelegate, UITextFieldDelegate ,NVActivityIndicatorViewable {
	
	@IBOutlet weak var emailParentTextfield: UITextField!
	@IBOutlet weak var emergencyContanctStudentTextField: UITextField!
	@IBOutlet weak var sexStudentSegCntrl: UISegmentedControl!
	@IBOutlet weak var lastNameStudentTextField: UITextField!
	@IBOutlet weak var nameStudentTextField: UITextField!
	@IBOutlet weak var birthDayTextField: UITextField!
    
    @IBOutlet weak var scrollView: UIScrollView!
	
    @IBOutlet weak var studentImage: UIImageView!
    
    //Variable de si es premium o no
    var planning: Planning!
    
    let validator = Validator()
	
	var group: GroupModel?
	var student: StudentModel?
	var bandEditStudent: Bool = false
    
    func requestForPayment() -> Void {
        
        //self.performSegue(withIdentifier: "payment", sender: self)
        let paymentController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "payment")
        self.navigationController?.pushViewController(paymentController, animated: true)
        
    }
	
	override func viewDidLoad() {
		
		super.viewDidLoad()
		setupView()
		
		if student != nil {
			bandEditStudent = true
			setupViewWithStudent(student!)
		}
		
	}
	
	func setupViewWithStudent(_ student: StudentModel) -> Void{
//Imagen que se descarga
        let urlImage = student.value(forKey: "imageUrl") as! String
        let url = URL(string: urlImage)
        KingfisherManager.shared.retrieveImage(with: url!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in

            if error == nil{
                self.studentImage.image = ImageUtils.resizeImage(image: image!, newWidth: 90.0)
                ImageUtils.circularImage(self.studentImage, color: ColorHelper.getThirdColor(1.0).cgColor)
            }

        })
		
		emergencyContanctStudentTextField.text = student.value(forKey: "emergencyContanct") as? String
		lastNameStudentTextField.text = student.value(forKey: "lastname") as? String
		nameStudentTextField.text = student.value(forKey: "name") as? String
        let segmentSex = (student.value(forKey: "sex") as! Int) == 1 ? 0 : 1
		sexStudentSegCntrl.selectedSegmentIndex = segmentSex
		birthDayTextField.text = student.value(forKey: "birthdate") as? String
		emailParentTextfield.text = student.value(forKey: "emailTutor") as? String
        
		
	}
	
	func setupView() -> Void{
		
		CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
		
		emergencyContanctStudentTextField.roundBorder()
		lastNameStudentTextField.roundBorder()
		nameStudentTextField.roundBorder()
		birthDayTextField.roundBorder()
		emailParentTextfield.roundBorder()
		
		createDatePicker()
		
		//REGISTER VALIDATION
		validator.registerField(emergencyContanctStudentTextField, rules: [PhoneNumberRule(message: "Teléfono a 10 digitos")])
		validator.registerField(lastNameStudentTextField, rules: [RequiredRule(message: "Ingresa los apellidos")])
		validator.registerField(nameStudentTextField, rules: [RequiredRule(message: "Ingresa el nombre")])
		validator.registerField(emailParentTextfield, rules: [EmailRule(message: "Ingresa un email válido")])
				
	}
	
	let datePicker = UIDatePicker()
	
	func createDatePicker(){
		
		datePicker.datePickerMode = .date
		
		let components = DateComponents()
		let maxDate = Calendar.current.date(byAdding: components, to: Date())
		
		datePicker.maximumDate = maxDate
		
		let toolbar  = UIToolbar()
		
		toolbar.sizeToFit()
		
		let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedForDatePicker))
		toolbar.setItems([doneButton], animated: false)
		
		birthDayTextField.inputAccessoryView = toolbar
		
		birthDayTextField.inputView = datePicker
		
	}
	
	func donePressedForDatePicker(){
		
		let dateFormatter = DateFormatter()
		//dateFormatter.dateStyle = .medium
		dateFormatter.dateFormat = "YYYY-MM-dd"
		//dateFormatter.timeStyle = .none
		
		birthDayTextField.text = "\(dateFormatter.string(from: datePicker.date))"
		self.view.endEditing(true)
		
	}
	
	
	@IBAction func saveStudentToDatabase(_ sender: Any) {
		
		if emailParentTextfield.text != ""{
			
			validator.validate(self)
			
		}
		else{
			
			validator.unregisterField(emailParentTextfield)
			validator.validate(self)
			
		}
		
	}
	
	var sex: Int = Constants.MALE
	
	@IBAction func changeSexSelection(_ sender:UISegmentedControl) {
		
		switch sender.selectedSegmentIndex
		{
		case 0:
			sex = Constants.MALE
		case 1:
			sex = Constants.FEMALE
		default:
			break;
		}
		
	}
	
	func validationSuccessful() {
		
		if self.bandEditStudent {
			
			self.bandEditStudent = false
			let name: String =  nameStudentTextField.text!
			let lastname: String = lastNameStudentTextField.text!
			let birthdate : String = birthDayTextField.text!
			let sex = self.sex
			let emergencyContanct = emergencyContanctStudentTextField.text!
			
			var params =
				[
					"name":name,
					"lastname":lastname,
					"birthday":birthdate,
					"gender":sex,
					"emergency_contact":emergencyContanct
					] as [String : Any]
			
			if (emailParentTextfield.text) != "" {
				params["email"] = emailParentTextfield.text
			}
			
			//GET IMAGE AND SAVE OBJECT
			self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
			
			let url = "\(Constants.STUDENT_ENDPOINT)\(student!.value(forKey: "idStudent") as! String)"
			
			Alamofire.request(url, method: .put, parameters: params)
				.validate()
				.response(completionHandler: { (dataResponse) in
					
					self.stopAnimating()
					
					if StudentController.editResponseFromServer(dataResponse){
						
						StudentController.editStudentInDatabase(student: self.student!, name: self.nameStudentTextField.text!, lastname: self.lastNameStudentTextField.text!, sex: self.sex, emergencyContact: self.emergencyContanctStudentTextField.text!, birthday: self.birthDayTextField.text!,email: self.emailParentTextfield.text)
						
                        
                        self.view.endEditing(true)
                        
						_ = SweetAlert().showAlert("Estudiante editado", subTitle: "¡Editaste un estudiante correctamente!", style: AlertStyle.success, buttonTitle:"Ok", buttonColor: ColorHelper.getGreenColor(1.0)) { (button) -> Void in
							
							self.navigationController?.popViewController(animated: true)
							
						}
					}else{
                        
                        self.view.endEditing(true)
						_ = SweetAlert().showAlert("Error", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
						
					}
					
					
					
				})

			
		}else{
            
            //Verifica si es premiun o no
            let url = "\(Constants.PAYMENT_ENDPOINT)\(UserController.getUserInDatabase()?.value(forKey: "idUser") as! String)"
            
            Alamofire.request(url, method: .get)
                .validate()
                .response(completionHandler: { (dataResponse) in
                    if let paymentObject = PaymentControllerSereyd.getPaymentStatus(dataResponse){
                        
                        if paymentObject.codeError == Constants.ACTIVE_SUSCRIPTION{
                            let name: String =  self.nameStudentTextField.text!
                            let lastname: String = self.lastNameStudentTextField.text!
                            let birthdate : String = self.birthDayTextField.text!
                                        let sex = self.sex
                            let emergencyContanct = self.emergencyContanctStudentTextField.text!
                            
                                        var params =
                                        [
                                            "name":name,
                                            "lastname":lastname,
                                            "birthday":birthdate,
                                            "gender":sex,
                                            "emergency_contact":emergencyContanct,
                                            "id_group": self.group?.value(forKey: "idGroup") as! String
                                        ] as [String : Any]
                            
                            if (self.emailParentTextfield.text) != "" {
                                params["email"] = self.emailParentTextfield.text
                                        }
                            
                                        //GET IMAGE AND SAVE OBJECT
                                        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
                            
                                        Alamofire.request(Constants.STUDENT_ENDPOINT, method: .post, parameters: params)
                                        .validate()
                                        .response(completionHandler: { (dataResponse) in
                            
                                            self.stopAnimating()
                            
                                            if let student = StudentController.getStudentByGroupFromResponse(dataResponse){
                            
                                                student.groups.append(self.group!)
                                                StudentController.addStudentToDatabase(student)
                                                self.view.endEditing(true)
                            
                                                _ = SweetAlert().showAlert("Alumno agregado", subTitle: "¡Agregaste un alumno correctamente!", style: AlertStyle.success, buttonTitle:"Ok", buttonColor: ColorHelper.getGreenColor(1.0)) { (button) -> Void in
                            
                                                    self.navigationController?.popViewController(animated: true)
                                                }
                                            }else{
                                                self.view.endEditing(true)
                                                _ = SweetAlert().showAlert("Error", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                            
                                            }
                            
                                        })
                            
                        }else if paymentObject.codeError == Constants.TEST_PERIOD{
                            
                            if paymentObject.resourceCount > 0{
                                
                                let prem = self.planning.value(forKey: "premium") as! Int
                                
                                if prem == Constants.ACTIVE_SUSCRIPTION {
                                    
                                    //                                    SweetAlert().showAlert("Descargar", subTitle: "Te quedan \(paymentObject.resourceCount!) recursos ¿La quieres descargar?", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor: ColorHelper.getRedColor(1.0), otherButtonTitle:  "Descargar", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                                    //                                        if isOtherButton == false {
                                    //
                                    //                                            super.viewDidLoad()
                                    //
                                    //                                        }
                                    //                                    }
                                    
                                }else{
                                    
                                    _ = SweetAlert().showAlert("Ops..", subTitle: "Este contenido es PREMIUM, suscríbete para poder acceder a más planeaciones y recursos.", style: AlertStyle.error)
                                    
                                }
                                
                            }else{
                                
                                //SUSCRUBIRTE
                                SweetAlert().showAlert("Aviso", subTitle: "Tu periodo de prueba ha finalizado, vuélvete PREMIUM para disfrutar de más planeaciones y recursos.", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor: ColorHelper.getRedColor(1.0), otherButtonTitle:  "Suscribirte", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                                    if isOtherButton == false {
                                        
                                        self.requestForPayment()
                                        
                                    }
                                }
                                
                            }
                            
                        }else if paymentObject.codeError == Constants.PAYMENT_REQUIRED{
                            
                            //SUSCRIBIRTE
                            SweetAlert().showAlert("Aviso", subTitle: "Tu suscripción ha caducado, vuélvete PREMIUM y disfruta de tus planeaciones y recursos.", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor: ColorHelper.getRedColor(1.0), otherButtonTitle:  "Suscribirte", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
                                if isOtherButton == false {
                                    
                                    self.requestForPayment()
                                    
                                }
                            }
                            
                        }else{
                            
                            _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                            
                        }
                        
                    }else{
                        
                        _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                        
                        
                    }
                })
			

				
		}
		
		
	}
	
	func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
		
		var stringError = ""
		
		for (field, error) in errors {
			if let field = field as? UITextField {
				field.layer.borderColor = UIColor.red.cgColor
				field.layer.borderWidth = 1.0
			}
			
			stringError = stringError + "\n\(error.errorMessage)"
			error.errorLabel?.text = error.errorMessage // works if you added labels
			error.errorLabel?.isHidden = false
			
		}
		
		_ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos! \(stringError)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
        self.view.endEditing(true)
		
	}
	
	
	
	//KEYBOARD METHODS
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		self.view.endEditing(true)
		
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		
		textField.resignFirstResponder()
		return true
		
	}
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == birthDayTextField || textField == emergencyContanctStudentTextField || textField == emailParentTextfield{
            
            scrollView.setContentOffset(CGPoint(x: 0, y: 250), animated: true)
            
        }
        
    }
    
    @IBAction func showParentEmailDescription(_ sender: Any) {
        
        SweetAlert().showAlert("Aviso", subTitle: "Saber más acerca del portal de padres", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor: ColorHelper.getGrayColor(1.0) , otherButtonTitle:  "Ir", otherButtonColor: ColorHelper.getRedColor(1.0)) { (isOtherButton) -> Void in
            if isOtherButton == false {
                
                
                
            }
        }
        
    }
	
}
