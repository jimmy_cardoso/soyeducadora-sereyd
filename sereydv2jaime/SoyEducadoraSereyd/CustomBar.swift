//
//  CustomBar.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 21/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import UIKit

class CustomBar{
	
    static var viewControllerCurrent: UIViewController!
	static var viewControllerToOpen: UIViewController!
	
	static func setNavBarProfile(_ navBar: UINavigationBar,navItem: UINavigationItem, nav : UIViewController) -> Void{
		
		SetCenteredImage(navItem)
		viewControllerCurrent = nav
		
	}
	
	static func SetCenteredImage(_ navItem: UINavigationItem){
		
        
		let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
		imageView.contentMode = .scaleAspectFit
		let image = UIImage(named: "launch_bar")
		imageView.image = image
		navItem.titleView = imageView
		
	}

}
