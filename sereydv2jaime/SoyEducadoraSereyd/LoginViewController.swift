//
//  LoginViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 19/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import SwiftValidator
import RealmSwift
import Alamofire
import AlamofireImage
import NVActivityIndicatorView

class LoginViewController: UIViewController , ValidationDelegate, UITextFieldDelegate,NVActivityIndicatorViewable {

	let validator = Validator()
	
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var labelTerms: UILabel!
	@IBOutlet weak var emailTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	
	override func viewDidLoad() {
		
		super.viewDidLoad()
        let gestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(LoginViewController.handleTap))
        labelTerms.isUserInteractionEnabled = true
        labelTerms.addGestureRecognizer(gestureRecognizer)
		setupView()
	
    }
    
    func handleTap(gestureRecognizer: UIGestureRecognizer) {
        openURL(Constants.TERMS_URL)
    }
    
    func openURL(_ urlString: String){
        let url = URL(string: urlString)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }

	func setupView() -> Void{
		
		emailTextField.roundBorder()
		passwordTextField.roundBorder()
		
		//REGISTER VALIDATION
		validator.registerField(emailTextField, rules: [EmailRule(message: "Escribe un email válido")])
		validator.registerField(passwordTextField, rules: [MinLengthRule(length: 6, message: "Contraseña con mínimo 6 caractéres")])
		
	}
	
	override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
	@IBAction func cancelOperation(_ sender: Any) {
		
		dismiss(animated: true, completion: nil)
		
	}
	
	@IBAction func loginWithOwnAccount(_ sender: Any) {
	
		validator.validate(self)
		
	}
	
	func validationSuccessful() {
		
		let params = ["email":emailTextField.text!,
		              "password":passwordTextField.text!]
		
		//GET IMAGE AND SAVE OBJECT
		self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
		
		Alamofire.request(Constants.LOGIN_ENDPOINT, method: .post, parameters: params)
			.validate()
			.responseString { (dataResponse) in
				
				self.stopAnimating()
				
				guard dataResponse.error == nil else{
					
					let stringError = AlamofireHandlerError.getErrorMessage(dataResponse)
					_ = SweetAlert().showAlert("Error", subTitle: "\(stringError)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
					
					return
					 
				}
				
				guard let dataResponseSign = dataResponse.data else{
					
					return
					
				}
				
				let jsonResponseUser = try? JSONSerialization.jsonObject(with: dataResponseSign, options: .allowFragments)
				
				guard (jsonResponseUser as? [String:Any]) != nil else{
					
					return
					
				}
				
                let user = UserController.getJsonFromResponse(jsonResponseUser as! NSDictionary)
                self.downloadForUserContent(user)
         		
		}
	}
    
    func downloadForUserContent(_ user: UserModel)-> Void{
        
        UserController.saveUserInDatabase(user: user)
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        Alamofire.request("\(Constants.GROUP_BYUSER_ENDPOINT)\(UserController.getUserInDatabase()!.value(forKey: "idUser")!)", method: .get)
            .validate()
            .response { (dataDefaultResponse) in
                
                self.stopAnimating()
                
                if let groupList = GroupController.getGroupListFromResponse(dataDefaultResponse) {
                    
                    GroupController.saveListUnrepeatedGroupInList(groupList,UserController.getUserInDatabase()!)
                    let groups = GroupController.getGroupInDatabase()
                    for group in groups{
                        
                        if let studentList = StudentController.getListStudentForGroup(dataDefaultResponse,group){
                            StudentController.saveListUnrepeatedStudentInList(studentList, group)
                        }
                        
                    }
                    
                    
                    self.dismiss(animated: true, completion: nil)
                    self.performSegue(withIdentifier: "mainMenuSegue", sender: self)

                                        
                    
                }
                
        }
        
        
    }

	
	func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
		
		var stringError = ""
		
		for (field, error) in errors {
			if let field = field as? UITextField {
				field.layer.borderColor = UIColor.red.cgColor
				field.layer.borderWidth = 1.0
			}
			
			stringError = stringError + "\n\(error.errorMessage)"
			error.errorLabel?.text = error.errorMessage // works if you added labels
			error.errorLabel?.isHidden = false
			
		}
		
        
        self.view.endEditing(true)
		_ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos! \(stringError)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
		
		
	}

	
	
	//KEYBOARD METHODS
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		self.view.endEditing(true)
		
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		
		textField.resignFirstResponder()
		return true
		
	}
	
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 150), animated: true)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
    }
	
    @IBAction func forgotPassword(_ sender: Any) {
        
        self.performSegue(withIdentifier: "forgotPasswordSegue", sender: self)
        
    }
}
