//
//  DiagnoseHistoryViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 12/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import MessageUI


class DiagnoseHistoryViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable, MFMailComposeViewControllerDelegate{
    
    var group : GroupModel?
    var diagnoseList = [[ResultDiagnose]]()
    var diagnoseInDatabase = [DiagnoseModel]()
    
    @IBOutlet weak var tableViewDiagnose: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        diagnoseInDatabase = DiagnoseController.getDiagnoseForGroup(group!)!
        downloadForDiagnoseResult(diagnoseInDatabase)
        
    }
    
    func downloadForDiagnoseResult(_ list: [DiagnoseModel]){
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        for diagnose in list{
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            let urlDiagnose = "\(Constants.DIAGNOSTE_COMPLETE_ENDPOINT)\(diagnose.value(forKey: "idGroupDiagnose") as! String)"
            Alamofire.request(urlDiagnose, method: .get)
                .validate()
                .response(completionHandler: { (dataResponse) in
                    
                    self.stopAnimating()
                    if let result = DiagnoseController.getDiagnoseResultFromResponse(dataResponse){
                        
                        self.diagnoseList.append(result)
                        self.tableViewDiagnose.reloadData()
                        
                    }
                    
                })
            
        }
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return diagnoseList.count
        
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DiagnoseResultTableViewCell
        
        cell.dateDiagnose.text = diagnoseInDatabase[indexPath.row].value(forKey: "date") as? String
        
        cell.label0.text = "\(String(describing: diagnoseList[indexPath.row][0].percent!)) %"
        cell.progres0.progress = Float(diagnoseList[indexPath.row][0].percent!)/100
        
        cell.label1.text = "\(String(describing: diagnoseList[indexPath.row][1].percent!)) %"
        cell.progres1.progress = Float(diagnoseList[indexPath.row][1].percent!)/100
        
        
        cell.label2.text = "\(String(describing: diagnoseList[indexPath.row][2].percent!)) %"
        cell.progres2.progress = Float(diagnoseList[indexPath.row][2].percent!)/100
        
        
        cell.label3.text = "\(String(describing: diagnoseList[indexPath.row][3].percent!)) %"
        cell.progres3.progress = Float(diagnoseList[indexPath.row][3].percent!)/100
        
        
        cell.label4.text = "\(String(describing: diagnoseList[indexPath.row][4].percent!)) %"
        cell.progres4.progress = Float(diagnoseList[indexPath.row][4].percent!)/100
        
        
        cell.label5.text = "\(String(describing: diagnoseList[indexPath.row][5].percent!)) %"
        cell.progres5.progress = Float(diagnoseList[indexPath.row][5].percent!)/100
        
        cell.tapExportAction = {(cell) in
            
            SweetAlert().showAlert("Exportar diagnóstico", subTitle: "Se exportará tu diagnóstico y se enviará vía correo electrónico.", style: AlertStyle.warning, buttonTitle:"Exportar", buttonColor: ColorHelper.getGreenColor(1.0), otherButtonTitle:  "Cancelar", otherButtonColor: ColorHelper.getRedColor(1.0)) { (isOtherButton) -> Void in
                if isOtherButton == true {
                    
                    let url = Constants.URL_SERVER + "/group_diagnostic/file/" + (self.diagnoseInDatabase[indexPath.row].value(forKey: "idGroupDiagnose") as! String)
                    
                    self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
                    
                    Alamofire.request(url, method: .get, parameters: nil)
                        .validate()
                        .response(completionHandler: { (dataResponse) in
                            
                            self.stopAnimating()
                            
                            if let evalFilePath = DiagnoseController.getDiagnoseFilePath(dataResponse){
                                
                                let url = URL(string: Constants.URL_SERVER + "/" + evalFilePath)
                                let request = NSMutableURLRequest(url: url!)
                                
                                let task = URLSession.shared.dataTask(with: request as URLRequest){
                                    data, response, error in
                                    
                                    if error != nil{
                                        
                                        DispatchQueue.main.sync(execute: {
                                            
                                            self.stopAnimating()
                                            _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                                            
                                        })
                                        
                                    }else{
                                        
                                        if let dataData = data{
                                            
                                            DispatchQueue.main.sync(execute: {
                                                
                                                self.stopAnimating()
                                                do{
                                                    
                                                    var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last as NSURL?
                                                    
                                                    let evaluationFileName = DiagnoseController.getFileNameForDiagnose(self.diagnoseInDatabase[indexPath.row])
                                                    docURL = docURL?.appendingPathComponent(evaluationFileName) as NSURL?
                                                    try dataData.write(to: docURL! as URL)
                                                    
                                                    //UPDATE UI
                                                    
                                                    let mailComposeViewController = self.configuredMailComposeViewController(self.diagnoseInDatabase[indexPath.row])
                                                    
                                                    if MFMailComposeViewController.canSendMail() {
                                                        
                                                        self.present(mailComposeViewController, animated: true, completion: nil)
                                                        
                                                    } else {
                                                        
                                                        self.showSendMailErrorAlert()
                                                        
                                                    }
                                                    
                                                }catch{
                                                    
                                                    _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                                                    
                                                }
                                                
                                            })
                                            
                                        }else{
                                            
                                            DispatchQueue.main.sync(execute: {
                                                
                                                self.stopAnimating()
                                                _ = SweetAlert().showAlert("Error", subTitle: "Algo salió mal , inténtalo de nuevo", style: AlertStyle.error)
                                                
                                            })
                                            
                                        }
                                        
                                    }
                                    
                                }
                                
                                task.resume()
                                
                                
                            }else{
                                
                                _ = SweetAlert().showAlert("Ooops!", subTitle: "Algo salió mal", style: AlertStyle.warning)
                                
                            }
                            
                        })
                    
                }
            }
            
        }
        
        return cell
        
    }
    
    func configuredMailComposeViewController(_ diagnose: DiagnoseModel) -> MFMailComposeViewController {
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        //mailComposerVC.setToRecipients(["nurdin@gmail.com"])
        mailComposerVC.setSubject("Diagnóstico")
        mailComposerVC.setMessageBody("PDF de Diagnóstico adjunto", isHTML: false)
        
        let pathFileWord = DiagnoseController.getFileForDiagnose(diagnose)
        let fileDataWord = NSData(contentsOf: pathFileWord!)
        let fileNameWord = DiagnoseController.getFileNameForDiagnose(diagnose)
        
        mailComposerVC.addAttachmentData(fileDataWord! as Data, mimeType: "application/vnd.openxmlformats-officedocument.wordprocessingml.document", fileName: fileNameWord)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        _ = SweetAlert().showAlert("No pudimos mandar el correo", subTitle: "Tu dispositivo no puede mandar email, configura una cuenta e inténtalo de nuevo.", style: AlertStyle.warning)
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
    }

}

