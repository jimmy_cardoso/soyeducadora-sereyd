//
//  QuestionTableViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 20/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class QuestionTableViewCell: UITableViewCell {

    
    @IBOutlet weak var typeQuestion: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var questionLabel: UITextView!

}
