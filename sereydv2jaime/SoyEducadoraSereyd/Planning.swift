//
//  File.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 13/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift

class Planning: Object {
    
    var idPlanning: String?
    var urlFile: String?
    var urlWord: String?
    var preview: String?
    var name: String?
    var descriptionPlanning: String?
    var imagePlanning: String?
    var field: String?
    var premium: Int = 0
    dynamic var user: UserModel?
    
}
