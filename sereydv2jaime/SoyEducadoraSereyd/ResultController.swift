//
//  ResultController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 12/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift

class ResultController {
    
    class func addResultToDatabase(_ result: ResultModel) -> Void{
        
        let realm = try! Realm()
        
        try! realm.write {
            
            realm.add(result)
            
        }
        
    }
    
    class func getResultForDiagnose(_ diagnose: DiagnoseModel) -> [ResultModel]?{
        
        var items: LinkingObjects<ResultModel>!
        items = diagnose.value(forKey: "results") as? LinkingObjects<ResultModel>
        
        var arrayResult = [ResultModel]()
        
        for result in items{
            
            arrayResult.append(result)
            
        }
        
        return arrayResult
        
    }

    
}
