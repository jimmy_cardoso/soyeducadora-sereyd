//
//  CooperationUIViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 31/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import RealmSwift
import Floaty
import ActionButton
import NVActivityIndicatorView
import Alamofire

class CooperationDetailViewController: UIViewController, UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable {
	
	//@IBOutlet weak var floatCooperation: Floaty!
	@IBOutlet weak var tableView: UITableView!
	
	var group: GroupModel?
	var cooperations:[CooperationModel] = [CooperationModel]()
	var actionButton: ActionButton!
	override func viewDidLoad() {
		
		super.viewDidLoad()
		setupView()
		
	}
	
	override func viewDidAppear(_ animated: Bool) {
		
		requestForCooperationsInServer(group!)
		
	}
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
    }
	
	func setupView() {
		
        
		cooperations = CooperationController.getCooperationInDatabase(group!)
		
		actionButton = ActionButton(attachedToView: self.view, items: [])
		actionButton.action = { button in
			self.performSegue(withIdentifier: "crudCooperationSegue", sender: self)
		}
		
		actionButton.setTitle("+", forState: UIControlState())
		actionButton.backgroundColor = ColorHelper.getSecondaryColor(1.0)
		
	}
	
	func requestForCooperationsInServer(_ group: GroupModel) -> Void{
		
		self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
		
		let url = "\(Constants.GROUP_ENDPOINT)\(String(describing: group.value(forKey: "idGroup")!))"
		Alamofire.request(url, method: .get)
			.validate().response { (defaultDataResponse) in
				
				self.stopAnimating()
				
				if let cooperations = CooperationController.getCooperationListFromResponse(defaultDataResponse){
                   
                    print(cooperations)
                    CooperationController.saveListUnrepeatedCooperationInList(cooperations,self.group!)
					self.cooperations = CooperationController.getCooperationInDatabase(self.group!)
					self.tableView.reloadData()
                }else{
                    
                    _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "Inténtalo de nuevo más tarde", style: AlertStyle.error)
                    
                }
		}
		
		
	}
	
	public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
		
		return cooperations.count
		
	}
	
	var didSelectCooperationToEdit = false
	var selectedCooperationToEdit : CooperationModel?
	
	public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CooperationTableViewCell
		
		cell.conceptLabel.text = cooperations[indexPath.row].value(forKey: "concept") as? String
		cell.amountLabel.text = "$\(String(describing: cooperations[indexPath.row].value(forKey: "account")!))"
		cell.textViewDescription.text = "\(String(describing: cooperations[indexPath.row].value(forKey: "subject")!))"
        
		cell.tapDeleteAction = { (cell) in
			
			_ = SweetAlert().showAlert("¿Eliminar esta cooperación?", subTitle: "", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor:ColorHelper.getGreenColor(1.0), otherButtonTitle:  "Eliminar",otherButtonColor: ColorHelper.getRedColor(1.0)) { (isOtherButton) -> Void in
				if isOtherButton != true {
					
					self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
					let url = "\(Constants.COOPERATION_ENDPOINT)\(self.cooperations[indexPath.row].value(forKey: "id_cooperation") as! String)"
					Alamofire.request(url, method: .delete)
					.validate()
					.response(completionHandler: { (defaultDataResponse) in
						
						self.stopAnimating()
						
						if CooperationController.deleteResponseFromServer(defaultDataResponse){
							
							CooperationController.deleteCooperationInDatabase(cooperation: self.cooperations[indexPath.row])
							
							self.cooperations = CooperationController.getCooperationInDatabase(self.group!)
							self.tableView.reloadData()
							
							_ = SweetAlert().showAlert("Cooperación eliminada!", subTitle: "Has eliminado correctamente una cooperación", style: AlertStyle.success)
							
						}else{
							
							_ = SweetAlert().showAlert("Error", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
							

						}
						
					})
					
					
				}
			}
			
		}
        
		
		cell.tapEditAction = { (cell) in
			
			self.selectedCooperationToEdit = self.cooperations[indexPath.row]
			self.didSelectCooperationToEdit = true
			self.performSegue(withIdentifier: "crudCooperationSegue", sender: self)
			
		}
		
		
		return cell
		
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		if segue.identifier == "crudCooperationSegue" && self.didSelectCooperationToEdit == true{
			
			let cooperationViewController = segue.destination as! CooperationCrudViewController
			cooperationViewController.group = self.group
			cooperationViewController.cooperation = self.selectedCooperationToEdit
			
		}else{
			
			let cooperationViewController = segue.destination as! CooperationCrudViewController
			cooperationViewController.group = self.group
			
		}
		
	}
	
	
}
