//
//  StudentViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 25/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import RealmSwift
import Floaty
import Alamofire
import ActionButton
import NVActivityIndicatorView
import Kingfisher

class StudentDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable {
    
    @IBOutlet weak var tableView: UITableView!
    
    var group: GroupModel?
    var items: [StudentModel] = [StudentModel]()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        getStudentsFromGroup()
        
    }
    
    var actionButton: ActionButton!
    
    func setupView() -> Void{
        
        actionButton = ActionButton(attachedToView: self.view, items: [])
        actionButton.action = { button in
            self.performSegue(withIdentifier: "crudStudentSegue", sender: self)
        }
        
        
        actionButton.setTitle("+", forState: UIControlState())
        actionButton.backgroundColor = ColorHelper.getSecondaryColor(1.0)
        
        items = StudentController.getStudentInDatabaseByGroup(group!)!
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return items.count
        
    }
    
    var selectedStudentToEdit : StudentModel?
    var didSelectStudentToEdit: Bool = false
    var studentSelected : StudentModel?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.studentSelected = self.items[indexPath.row]
        self.performSegue(withIdentifier: "studentDetailVC", sender: self)
        
    }
    
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! StudentTableViewCell
        
        let studentSample: StudentModel = self.items[indexPath.row]
        //MARK:
        let name = "\(studentSample.value(forKey: "lastname") as! String)"
        let lastnames = "\(studentSample.value(forKey: "name") as! String)"
        //let completeName = "\(studentSample.value(forKey: "lastname") as! String) \(studentSample.value(forKey: "name") as! String)"
        cell.nameStudentLabel.text = name
        cell.lastnameStudentLabel.text = lastnames
        let urlImage = studentSample.value(forKey: "imageUrl") as! String
        let url = URL(string: urlImage)
        KingfisherManager.shared.retrieveImage(with: url!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
            
            if error == nil{
                
                cell.imageView?.image = ImageUtils.resizeImage(image: image!, newWidth: 40.0)
                
            }
            
        })
        cell.tapDeleteAction = { (cell) in
            
            _ = SweetAlert().showAlert("¿Eliminar este alumno?", subTitle: "", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor:ColorHelper.getGreenColor(1.0), otherButtonTitle:  "Eliminar",otherButtonColor: ColorHelper.getRedColor(1.0)) { (isOtherButton) -> Void in
                if isOtherButton != true {
                    
                    self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
                    
                    let url = "\(Constants.STUDENT_ENDPOINT)\(String(describing: self.items[indexPath.row].value(forKey: "idStudent")!))"
                    Alamofire.request(url, method: .delete)
                        .validate()
                        .response(completionHandler: { (dataResponse) in
                            
                            self.stopAnimating()
                            
                            let boolResponse = StudentController.getResponseFromDeleteStudent(dataResponse)
                            if boolResponse{
                                
                                StudentController.deleteStudentInDatabase(student: self.items[indexPath.row])
                                self.items = StudentController.getStudentInDatabaseByGroup(self.group!)!
                                
                                self.tableView.reloadData()
                                _ = SweetAlert().showAlert("Estudiante eliminado!", subTitle: "Has eliminado correctamente un estudiante", style: AlertStyle.success)
                                
                            }else{
                                _ = SweetAlert().showAlert("Ooops...", subTitle: "¡Algo salío mal, Inténtalo de nuevo!", style: AlertStyle.error)
                            }
                            
                        })
                    
                }
            }
            
            
        }
        
        cell.tapEditAction = { (cell) in
            
            self.selectedStudentToEdit = self.items[indexPath.row]
            self.didSelectStudentToEdit = true
            self.performSegue(withIdentifier: "crudStudentSegue", sender: self)
            
        }
        
        return cell
        
    }
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == "crudStudentSegue" && self.didSelectStudentToEdit == true {
            
            let studentViewController = segue.destination as! StudentCrudViewController
            studentViewController.group = self.group
            studentViewController.student = self.selectedStudentToEdit
            self.didSelectStudentToEdit = false
            
        }else if segue.identifier == "cooperationSegue"{
            
            let cooperationViewController = segue.destination as! CooperationDetailViewController
            cooperationViewController.group = self.group
            
        }else if segue.identifier == "studentDetailVC"{
            
            let studentProfileViewController = segue.destination as! StudentProfileDetailViewController
            studentProfileViewController.student = self.studentSelected
            
        }else{
            
            let studentViewController = segue.destination as! StudentCrudViewController
            studentViewController.group = self.group
            
        }
        
    }
    
    func getStudentsFromGroup() -> Void{
        
        //GET IMAGE AND SAVE OBJECT
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        Alamofire.request("\(Constants.GROUP_ENDPOINT)\(String(describing: group!.value(forKey: "idGroup")!))", method: .get)
            .validate()
            .response { (defaultDataResponse) in
                
                self .stopAnimating()
                
                if let students = StudentController.getStudentListFromResponse(defaultDataResponse){
                    
                    StudentController.saveListUnrepeatedStudentInList(students,self.group!)
                    self.items = StudentController.getStudentInDatabaseByGroup(self.group!)!
                    self.tableView.reloadData()
                    
                    
                }else{
                    
                    
                    _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "Inténtalo de nuevo más tarde", style: AlertStyle.error)
                    
                }
        }
        
    }
    
    
    
    
}
