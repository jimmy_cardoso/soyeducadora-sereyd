//
//  StudentProfileDetailViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 31/07/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class StudentProfileDetailViewController: UIViewController, NVActivityIndicatorViewable {
    
    @IBOutlet weak var labelNameStudent: UILabel!
    @IBOutlet weak var performance: UILabel!
    @IBOutlet weak var diagnose: UILabel!
    @IBOutlet weak var attendance: UILabel!
    @IBOutlet weak var behavior: UILabel!
    @IBOutlet weak var labelLanguage: UILabel!
    @IBOutlet weak var progressLanguage: UIProgressView!
    @IBOutlet weak var labelMath: UILabel!
    @IBOutlet weak var progressMath: UIProgressView!
    @IBOutlet weak var progressHealth: UIProgressView!
    @IBOutlet weak var progressSocial: UIProgressView!
    @IBOutlet weak var labelWorld: UILabel!
    @IBOutlet weak var progressWorld: UIProgressView!
    @IBOutlet weak var labelSocial: UILabel!
    
    @IBOutlet weak var progressArt: UIProgressView!
    @IBOutlet weak var labelArt: UILabel!
    @IBOutlet weak var labelHealth: UILabel!
    
    var student: StudentModel?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
        let name = "\(student?.value(forKey: "name") as! String) \(student?.value(forKey: "lastname") as! String)"
        labelNameStudent.text = name
        
        downloadUserInformation()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func downloadUserInformation() {
        
        
        //GET IMAGE AND SAVE OBJECT
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let url = "\(Constants.STUDENT_ENDPOINT_INFO)\(student!.value(forKey: "idStudent") as! String)"
        
        Alamofire.request(url, method: .get)
            .validate()
            .response { (dataResponse) in
                
                self.stopAnimating()
                
                if let dictInfoStudent = StudentController.getInfoStudentFromResponse(dataResponse){
                    
                    if let informationStudent = StudentController.getInformationByStudent(self.student!){
                        
                        StudentController.updateInformationForStudent(dictInfoStudent,informationStudent)
                        
                    }else{
                        
                        dictInfoStudent.student = self.student
                        StudentController.addInformationStudentToDatabase(dictInfoStudent)
                        
                    }
                }
                
                self.updateViewForInformation()
            
        }
        
    }
    
    func updateViewForInformation() -> Void{
        
        if let informationStudent = StudentController.getInformationByStudent(self.student!){
            
            self.performance.text = "\(round(informationStudent.value(forKey: "schoolPerformance") as! Double))"
            self.diagnose.text = "\(round(informationStudent.value(forKey: "diagnostic") as! Double)) pts"
            self.attendance.text = "\(informationStudent.value(forKey: "attendanceAccount") as! Int)"
            self.behavior.text = "\(informationStudent.value(forKey: "descriptionBehavior") as! String)"
            
            labelLanguage.text = "\(informationStudent.value(forKey: "language") as! Double)"
            progressLanguage.progress = informationStudent.value(forKey: "language") as! Float / 100
            
            labelMath.text = "\(informationStudent.value(forKey: "math") as! Double)"
            progressMath.progress = informationStudent.value(forKey: "math") as! Float / 100
            
            labelWorld.text = "\(informationStudent.value(forKey: "world") as! Double)"
            progressWorld.progress = informationStudent.value(forKey: "world") as! Float / 100
            
            labelSocial.text = "\(informationStudent.value(forKey: "social") as! Double)"
            progressSocial.progress = informationStudent.value(forKey: "social") as! Float / 100
            
            labelArt.text = "\(round(informationStudent.value(forKey: "art") as! Double))"
            progressArt.progress = informationStudent.value(forKey: "art") as! Float / 100

            labelHealth.text = "\(round(informationStudent.value(forKey: "health") as! Double))"
            progressHealth.progress = (informationStudent.value(forKey: "health") as! Float / 100)
            
        }
        
        
    }
    
    @IBAction func callContactEmergency(_ sender: Any) {
        
        
        //Chaining alerts with messages on button click
        _ = SweetAlert().showAlert("Aviso", subTitle: "Llamarás al contacto de emergencia de este alumno", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor:ColorHelper.getRedColor(1.0), otherButtonTitle:  "Llamar", otherButtonColor: ColorHelper.getGreenColor(1.0)) { (isOtherButton) -> Void in
            if isOtherButton == false {
                
                let phoneNumber = self.student!.value(forKey: "emergencyContanct") as! String
                if let url = URL(string: "tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
                
            }
        }
        
        
        
    }
    
}
