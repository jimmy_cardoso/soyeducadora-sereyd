//
//  ResourceDetailTableViewCell.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 04/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit

class ResourceDetailTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameFile: UILabel!
    @IBOutlet weak var iconDownload: UIImageView!

}
