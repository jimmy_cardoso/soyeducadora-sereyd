//
//  ForumDetailViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 19/12/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import Alamofire
import Cosmos
import Kingfisher
import NVActivityIndicatorView

class ForumDetailViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, NVActivityIndicatorViewable, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var textFieldComment: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UITextView!
    
    var array : [Answer] = [Answer]()
    var model : ForumModel?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        textFieldComment.roundBorder()
        
        textFieldComment.delegate = self
        
        labelTitle.text = model!.title!
        labelDescription.text = model!.question!
        
    }
    

    override func viewWillAppear(_ animated: Bool) {
        
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    func getDiscussion(){
        
        Alamofire.request(Constants.URL_DISCUSSION + "/\(self.model!.idDiscussion!)", method: .get).validate()
            .response { (dataResponse) in
                
                if let arrayResponse = ForumController.getAnswerList(dataResponse){
                    
                    self.array.removeAll()
                    self.array.append(contentsOf: arrayResponse)
                    self.tableView.reloadData()
                    
                }
                
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        self.getDiscussion()
        
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        (cell.viewWithTag(1) as? UITextView)?.text = self.array[indexPath.row].answer!
        let imageView = cell.viewWithTag(2) as! UIImageView
        (cell.viewWithTag(3) as? CosmosView)?.rating = Double(self.array[indexPath.row].ranking!)
        
        let url = URL(string: self.array[indexPath.row].image!)
        imageView.kf.setImage(with: url)
        ImageUtils.circularImage(imageView, color: ColorHelper.getSecondaryColor(1.0).cgColor)
        
        return cell
        
    }
    
    //segueRankAnswerSegue
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "segueRankAnswerSegue", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueRankAnswerSegue" {
        
            let vc = segue.destination as! RankAnswerViewController
            vc.answer = self.array[self.tableView.indexPathForSelectedRow!.row]
            
        }
        
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return array.count
        
    }

    
    //KEYBOARD METHODS
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    
    @IBAction func sendDiscussion(_ sender: Any) {
        
        if self.textFieldComment.text == "" {
            
            _ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
            
        }else{
            
            let idUser = UserController.getUserInDatabase()?.value(forKey: "idUser") as! String
            let paramsToSend = ["answer":textFieldComment.text!,"id_user":idUser,"id_discussion":self.model!.idDiscussion!] as [String : Any]
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            print(paramsToSend)
            Alamofire.request(Constants.URL_ANSWER, method: .post, parameters: paramsToSend)
                .validate()
                .response { (dataResponse) in
                    
                    self.stopAnimating()
                    
                    self.textFieldComment.text = ""
                    self.view.endEditing(true)
                    
                    if DiaryController.getDiaryResponseCreation(dataResponse){
                        
                        _ = SweetAlert().showAlert("Enviada exitosamente", subTitle: "Agradecemos tu respuesta", style: AlertStyle.success, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0)){ (isOtherButton) -> Void in
                            
                            self.getDiscussion()
                        }
                        
                    }else{
                        
                        _ = SweetAlert().showAlert("Ooops!", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                        
                    }
                    
            }
            
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
