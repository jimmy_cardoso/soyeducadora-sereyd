//
//  PaymentInformationViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 01/01/18.
//  Copyright © 2018 FullStack. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SwiftyButton

class PaymentInformationViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var paymentInformationView: UIView!
    @IBOutlet weak var testInformationView: UIView!
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var descriptionPaymentStatus: UILabel!
    @IBOutlet weak var textViewDescriptionPaymentLabel: UILabel!
    @IBOutlet weak var paymentInformationText: UITextView!
    @IBOutlet weak var buttonCancelSuscription: FlatButton!
    
    @IBOutlet weak var labelDateTest: UILabel!
    var paymentObject: PaymentInformation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        
        requestPaymentInformation()
        
    }
    
    func requestPaymentInformation() {
        
        self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        let url = Constants.PAYMENT_ENDPOINT + "\(UserController.getUserInDatabase()?.value(forKey: "idUser") as! String)"
        
        Alamofire.request(url, method: .get)
        .validate()
        .response { (dataResponse) in
            
            self.stopAnimating()
            if let paymentObjectServer = PaymentControllerSereyd.getPaymentObject(dataResponse){
                
                self.paymentObject = paymentObjectServer
                self.textViewDescriptionPaymentLabel.text = self.paymentObject.description!
                
                if self.paymentObject.code == Constants.ACTIVE_SUSCRIPTION {
                    
                    
                    self.paymentInformationView.isHidden = false
                    self.testInformationView.isHidden = true
                    
                    
                    self.descriptionPaymentStatus.text = self.paymentObject.descriptionCard
                    self.buttonCancelSuscription.isHidden = false
                    self.labelDate.text = "Fecha: " + self.paymentObject.initDate! + " a " + self.paymentObject.finishDate!
                    
                    var strDescriptionPaymentInfo = ""
                    
                    if self.paymentObject.cardNumber != nil {
                        
                        strDescriptionPaymentInfo = self.paymentObject.cardNumber! + "\n" + self.paymentObject.holderName! + "\n" + "\(self.paymentObject.amount) MXN"
                        
                    }else{
                        
                        if self.paymentObject.cancelled == Constants.CANCELLED {
                            
                            strDescriptionPaymentInfo = "Sin suscripción\nCancelaste tu suscripción"
                            self.buttonCancelSuscription.isHidden = true
                            
                        }else{
                            
                            strDescriptionPaymentInfo = "Tu forma de pago es con PayPal."
                            
                        }
                        
                    }
                    
                    self.paymentInformationText.text = strDescriptionPaymentInfo
                    
                }else if self.paymentObject.code == Constants.TEST_PERIOD {
                    
                    self.paymentInformationView.isHidden = true
                    self.testInformationView.isHidden = false
                    
                    self.labelDateTest.text = "Tu período de prueba finaliza\n\(self.paymentObject.initDate!) a \(self.paymentObject.finishDate!)"
                    
                    
                }else if self.paymentObject.code == Constants.PAYMENT_REQUIRED{
                    
                    
                    self.paymentInformationView.isHidden = true
                    self.testInformationView.isHidden = false
                    
                    self.labelDateTest.text = "Suscríbete para poder disfrutar de muchos beneficios"
                    
                }
                
            }else{
                
                _ = SweetAlert().showAlert("Oops!", subTitle: "Algo salió mal", style: AlertStyle.warning)
                
            }
            
        }
        
    }
    
    
    @IBAction func cancelSuscription(_ sender: Any) {
        
        _ = SweetAlert().showAlert("Cancelar suscripción", subTitle: "¿Quieres cancelar tu suscripción?", style: AlertStyle.warning, buttonTitle:"No", buttonColor: ColorHelper.getGrayColor(1.0) , otherButtonTitle:  "Cancelar", otherButtonColor: ColorHelper.getRedColor(1.0)) { (isOtherButton) -> Void in
            
            if isOtherButton == false {
                
                self.requesstForCancelSuscription()
                
            }
        }

        
    }
    
    func requesstForCancelSuscription(){
        
        
        if self.paymentObject.cardNumber != nil {
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            
            Alamofire.request(Constants.CANCEL_SUSCRIPTION_ENDPOINT + (UserController.getUserInDatabase()?.value(forKey: "idUser") as! String), method: .post)
            .validate()
            .response(completionHandler: { (dataResponse) in
                self.stopAnimating()
                if dataResponse.error == nil {
                    
                    if dataResponse.response?.statusCode == 200 {
                        
                        _ = SweetAlert().showAlert("Suscripción cancelada", subTitle: "Cancelaste tu suscripción a SEREYD.", style: AlertStyle.success, buttonTitle:"No", buttonColor: ColorHelper.getGrayColor(1.0)) { (isOtherButton) -> Void in
                            
                            if isOtherButton == false {
                                
                                self.requestPaymentInformation()
                                
                            }
                        }
                        
                        
                    }else{
                        
                        _ = SweetAlert().showAlert("Ops..", subTitle: "Algo salió mal.", style: AlertStyle.warning)
                    }
                    
                }else{
                    
                    _ = SweetAlert().showAlert("Ops..", subTitle: "Algo salió mal.", style: AlertStyle.warning)
                    
                }
                
            })
            
        }else{
            
            _ = SweetAlert().showAlert("Suscripción con paypal", subTitle: "No es necesario cancelar la suscripción.", style: AlertStyle.warning)
            
        }
        
        
    }
    
    @IBAction func suscribe(_ sender: Any) {
        
        
        //self.performSegue(withIdentifier: "paymentVC", sender: self)
        let paymentController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: /*"showPaymentInformation"*/"payment")
        self.navigationController?.pushViewController(paymentController, animated: true)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
