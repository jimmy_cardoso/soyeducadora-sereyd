//
//  QuestionViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 19/12/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import SwiftValidator
import Alamofire
import NVActivityIndicatorView

class QuestionViewController: UIViewController, NVActivityIndicatorViewable, ValidationDelegate, UITextFieldDelegate,UITextViewDelegate {

    @IBOutlet weak var textFieldDescription: UITextView!
    @IBOutlet weak var textFieldTitle: UITextField!
    
    let validator = Validator()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
        
        textFieldDescription.roundBorder()
        textFieldTitle.roundBorder()
        
        textFieldTitle.delegate = self
        textFieldDescription.delegate = self
        
        
        validator.registerField(textFieldTitle, rules: [RequiredRule(message: "Escribe el titúlo")])
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //KEYBOARD METHODS
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        var stringError = ""
        
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            
            stringError = stringError + "\n\(error.errorMessage)"
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
            
        }
        
        self.view.endEditing(true)
        _ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos! \(stringError)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
        
        
    }
    
    
    func validationSuccessful() {
        
        if self.textFieldDescription.text == "" {
            
            _ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
            
        }else{
            
            let idUser = UserController.getUserInDatabase()?.value(forKey: "idUser") as! String
            let paramsToSend = ["title":textFieldTitle.text!,"question":textFieldDescription.text,"id_user":idUser] as [String : Any]
            
            self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
            
            Alamofire.request(Constants.URL_DISCUSSION, method: .post, parameters: paramsToSend)
                .validate()
                .response { (dataResponse) in
                    
                    self.stopAnimating()
                    
                    self.textFieldTitle.text = ""
                    self.textFieldDescription.text = ""
                    
                    self.view.endEditing(true)
                    
                    if DiaryController.getDiaryResponseCreation(dataResponse){
                        
                        _ = SweetAlert().showAlert("Ok", subTitle: "Pregunta creada", style: AlertStyle.success, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0)){ (isOtherButton) -> Void in
                            
                            self.navigationController?.popViewController(animated: true)
                            
                        }
                        
                        
                    }else{
                        
                        _ = SweetAlert().showAlert("Ooops!", subTitle: "¡Algo salió mal!", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                        
                    }
                    
            }
            
        }
    }
    

    @IBAction func sendQuestion(_ sender: Any) {
        
        validator.validate(self)
        
    }
}
