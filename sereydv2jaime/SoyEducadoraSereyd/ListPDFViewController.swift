//
//  ListPDFViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Jimmy Cardoso on 04/07/18.
//  Copyright © 2018 FullStack. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import PDFReader
import NVActivityIndicatorView
import MessageUI

class ListPDFViewController: UIViewController, NVActivityIndicatorViewable , MFMailComposeViewControllerDelegate{
    
//    let remotePDFDocumentURLPath = "http://devstreaming.apple.com/videos/wwdc/2016/201h1g4asm31ti2l9n1/201/201_internationalization_best_practices.pdf"
//    let remotePDFDocumentURL = URL(string: remotePDFDocumentURLPath)!
//    let document = PDFDocument(url: remotePDFDocumentURL)!
    
    

    //    @IBOutlet weak var listPDFAsistencia: WKWebView!
//
//    var webView: WKWebView!
//
//    override func loadView() {
//        super.loadView()
////        let webConfiguration = WKWebViewConfiguration()
////        webView = WKWebView(frame: .zero, configuration: webConfiguration)
////        webView.uiDelegate = self as? WKUIDelegate
////        view = webView
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let remotePDFDocumentURLPath = "https://app.soyeducadorasereyd.com/group/information/1388"
        let remotePDFDocumentURL = URL(string: remotePDFDocumentURLPath)!
        //let document = PDFDocument(url: remotePDFDocumentURL)!
        if let doc = document(remotePDFDocumentURL) {
            showDocument(doc)
    
//        let myURL = URL(string: "https://app.soyeducadorasereyd.com/group/information/1388")
//        let myRequest = URLRequest(url: myURL!)
//        webView.load(myRequest)
        }
    }
    // Initializes a document with the remote url of the pdf
    fileprivate func document(_ remoteURL: URL) -> PDFDocument? {
        return PDFDocument(url: remoteURL)
    }
    
    fileprivate func showDocument(_ document: PDFDocument) {
        
        let controller = PDFViewController.createNew(with: document)
        navigationController?.pushViewController(controller, animated: true)
    }
    

//    func setupView() -> Void{
//        
//        CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
//        //navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(GroupViewController.addGroup(_:)))
//        //items = GroupController.getGroupInDatabase()
//    }


//    // Initializes a document with the remote url of the pdf
//    fileprivate func document(_ remoteURL: URL) -> PDFDocument? {
//        return PDFDocument(url: remoteURL)
//    }
//
//    fileprivate func showDocument(_ document: PDFDocument) {
//
//        let controller = PDFViewController.createNew(with: document)
//        navigationController?.pushViewController(controller, animated: true)
//    }
//
//
//    func shareByEmailPlanning() -> Void{
//        
//        if PlanningController.isPlanningInDatabase(planning) && PlanningController.isPlanningFileInDisk(planning){
//            
//            let mailComposeViewController = configuredMailComposeViewController()
//            
//            if MFMailComposeViewController.canSendMail() {
//                
//                self.present(mailComposeViewController, animated: true, completion: nil)
//                
//            } else {
//
//                self.showSendMailErrorAlert()
//
//            }
//            
//        }else{
//
//            _ = SweetAlert().showAlert("Aviso", subTitle: "Adquiere la planeación y descárgala para poder compartirla", style: AlertStyle.warning)
//            
//            
//        }
//
//        
//    }
//
//    func configuredMailComposeViewController() -> MFMailComposeViewController {
//        
//        let mailComposerVC = MFMailComposeViewController()
//        mailComposerVC.mailComposeDelegate = self
//        
//        //mailComposerVC.setToRecipients(["nurdin@gmail.com"])
//        mailComposerVC.setSubject("Planeación")
//        mailComposerVC.setMessageBody("PDF de planeación adjunto", isHTML: false)
//
//        let pathFile = PlanningController.getFileForPlanning(planning)
//        let fileData = NSData(contentsOf: pathFile!)
//        let fileName = PlanningController.getFileNameForPlanning(planning)
//        
//        let pathFileWord = PlanningController.getFileWordForPlanning(planning)
//        let fileDataWord = NSData(contentsOf: pathFileWord!)
//        let fileNameWord = PlanningController.getFileNameForPlanningWord(planning)
//        
//        mailComposerVC.addAttachmentData(fileDataWord! as Data, mimeType: "application/vnd.openxmlformats-officedocument.wordprocessingml.document", fileName: fileNameWord)
//        mailComposerVC.addAttachmentData(fileData! as Data, mimeType: "application/pdf ", fileName: fileName)
//        
//        return mailComposerVC
//    }
//
//    func showSendMailErrorAlert() {
//        
//        _ = SweetAlert().showAlert("No pudimos mandar el correo", subTitle: "Tu dispositivo no puede mandar email, configura una cuenta e inténtalo de nuevo.", style: AlertStyle.warning)
//
//    }
//
//    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
//        controller.dismiss(animated: true, completion: nil)
//        
//    }
}
