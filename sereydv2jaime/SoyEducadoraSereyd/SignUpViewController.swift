//
//  SignUpViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 18/05/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import Foundation
import SwiftValidator
import SwiftyButton
import Alamofire
import NVActivityIndicatorView

class SignUpViewController: UIViewController, ValidationDelegate, UITextFieldDelegate,NVActivityIndicatorViewable {
	
	
    @IBOutlet weak var labelTerms: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
	//@IBOutlet weak var passwordConfirmTextField: UITextField!
	//@IBOutlet weak var passwordTextField: UITextField!
	@IBOutlet weak var nameTextField: UITextField!
	@IBOutlet weak var emailTextField: UITextField!
	
	
	let validator = Validator()
	
	override func viewDidLoad()
	{
		
		super.viewDidLoad()
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(SignUpViewController.handleTap))
        labelTerms.isUserInteractionEnabled = true
        labelTerms.addGestureRecognizer(gestureRecognizer)
        
		setupView()
		
	}
    
    func handleTap(gestureRecognizer: UIGestureRecognizer) {
        openURL(Constants.TERMS_URL)
    }
    
    func openURL(_ urlString: String){
        let url = URL(string: urlString)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
	
	func setupView(){
		
		CustomBar.setNavBarProfile((self.navigationController?.navigationBar)!, navItem: navigationItem,nav: self)
		emailTextField.roundBorder()
		nameTextField.roundBorder()
		//passwordTextField.roundBorder()
		//passwordConfirmTextField.roundBorder()
		
		
		//REGISTER VALIDATION
		validator.registerField(nameTextField, rules: [FullNameRule(message: "Escribe tu nombre completo")])
		validator.registerField(emailTextField, rules: [EmailRule(message: "Escribe un email válido")])
		//validator.registerField(passwordTextField, rules: [MinLengthRule(length: 6, message: "Contraseña con mínimo 6 caractéres")])
		//validator.registerField(passwordConfirmTextField, rules: [ConfirmationRule(confirmField: passwordTextField, message: "Las contraseñas no coinciden")])
		
		
	}
	
	func validationSuccessful() {
		
		let params = ["name":nameTextField.text!,
		              "email":emailTextField.text!]
		              //"password":passwordTextField.text!
		
		//GET IMAGE AND SAVE OBJECT
		self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
		
		Alamofire.request(Constants.USER_ENDPOINT, method: .post, parameters: params)
		.validate()
		.responseString { (dataResponse) in
			
			self.stopAnimating()
			
			guard dataResponse.error == nil else{
				
                if let stringErrorUpdate = AlamofireHandlerError.getErrorUpdateMessage(dataResponse)
                {
                    _ = SweetAlert().showAlert("Error", subTitle: "\(stringErrorUpdate)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                    
                }else{
                    
                    let stringError = AlamofireHandlerError.getErrorMessage(dataResponse)
                    _ = SweetAlert().showAlert("Error", subTitle: "\(stringError)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
                }
				
				
				return
				
			}
			
			guard let dataResponseSign = dataResponse.data else{
				
				return
				
			}
			
			let jsonResponseCredential = try? JSONSerialization.jsonObject(with: dataResponseSign, options: .allowFragments)
			
			guard (jsonResponseCredential as? [String:Any]) != nil else{
				
                return
				
			}
			
			_ = SweetAlert().showAlert("Registro exitoso", subTitle: "¡Verifica tu correo , te adjuntamos la contraseña para poder accesar a la app!", style: AlertStyle.success, buttonTitle:"Ok", buttonColor: ColorHelper.getGreenColor(1.0)) { (button) -> Void in
				
				self.dismiss(animated: true, completion: nil)
				
			}
			
		}
	}

	func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
		
		var stringError = ""
		
		for (field, error) in errors {
			if let field = field as? UITextField {
				field.layer.borderColor = UIColor.red.cgColor
				field.layer.borderWidth = 1.0
			}
			stringError = stringError + "\n\(error.errorMessage)"
			error.errorLabel?.text = error.errorMessage // works if you added labels
			error.errorLabel?.isHidden = false
			
		}
		
        
        self.view.endEditing(true)
		_ = SweetAlert().showAlert("Error", subTitle: "¡Ingresa los datos requeridos! \(stringError)", style: AlertStyle.error, buttonTitle:"Ok", buttonColor:ColorHelper.getGreenColor(1.0))
		
		
	}
	
	//KEYBOARD METHODS
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		self.view.endEditing(true)
		
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		
		textField.resignFirstResponder()
		return true
		
	}
	
	
	
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 50), animated: true)
        
        
    }
    
	@IBAction func signUpClicked(_ sender: Any) {
		
		validator.validate(self)
		
	}
	
	@IBAction func cancelOperation(_ sender: Any) {
	
		dismiss(animated: true, completion: nil)
		
	}
}
