//
//  AlertController.swift
//  SoyEducadoraSereyd
//
//  Created by Jimmy Cardoso on 22/06/18.
//  Copyright © 2018 FullStack. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire

class AlertController{
    
    class func addAlertToDatabase(_ alert: AlertModel) -> Void{
        
        let realm = try! Realm()
        
        try! realm.write {
            
            
            realm.add(alert)
            
        }
        
    }
    
    class func deleteAlertInDatabase(alert: AlertModel) -> Void{
        
        let realm = try! Realm()
        
        // Delete an object with a transaction
        try! realm.write {
            realm.delete(alert)
        }
        
    }
    
    class func editAlertInDatabase(alert: AlertModel, name: String,descriptionAlert: String,date: String, time: String) -> Void{
        
        let realm = try! Realm()
        
        // Delete an object with a transaction
        try! realm.write {
            
            alert.setValue(name, forKey: "name")
            alert.setValue(descriptionAlert, forKey: "descriptionAlert")
            alert.setValue(date, forKey: "date")
            alert.setValue(time, forKey: "time")
            
        }
        
    }
    
    class func alertFromResponse(_ dataResponse: DefaultDataResponse) -> AlertModel?{
        
        guard dataResponse.error == nil else{
            
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        let alertObject = jsonResponse!["alert"] as! [String:Any]
        
        let alert = AlertModel()
        alert.name = alertObject["name"] as? String
        alert.descriptionAlert = alertObject["descriptionAlert"] as? String
        alert.date = alertObject["date"] as? String
        //alert.time = alertObject["time"] as? String
        
        return alert
    }
    
    class func getAlertInDatabase(_ group: GroupModel) -> [AlertModel]{
        
        var items: LinkingObjects<AlertModel>!
        items = group.value(forKey: "alerts") as! LinkingObjects<AlertModel>
        
        var alert = [AlertModel]()
        
        for alertDb in items{
            
            alert.append(alertDb)
            
        }
        
        return alert
        
    }
    
    class  func getAlertListFromResponse(_ dataResponse: DefaultDataResponse) -> [AlertModel]?{
        
        
        guard dataResponse.error == nil else{
            
            return nil
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return nil
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return nil
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return nil
            
        }
        
        guard let groupObject = jsonResponse?["data"] as? [String:Any] else{
            
            return nil
        }
        
        guard let alertArrayRaw = groupObject["alerts"] as? NSArray else{
            
            return nil
            
        }
        
        var alertArray = [AlertModel]()
        
        if alertArrayRaw.count == 0{
            return alertArray
        }
        
        for alertRaw in alertArrayRaw{
            
            let alertObject = alertRaw as! [String:Any]
            
            let alertAppend = AlertModel()
            alertAppend.name = alertObject["name"] as? String
            alertAppend.descriptionAlert = alertObject["descriptionAlert"] as? String
            alertAppend.date = alertObject["date"] as? String
            alertAppend.time = alertObject["time"] as? String
            
            alertArray.append(alertAppend)
            
        }
        
        return alertArray
        
    }
    
    class func saveListUnrepeatedAlertInList(_ alertArray: [AlertModel],_ group: GroupModel){
        
        let listAlertInDatabase = getAlertInDatabase(group)
        
        if  alertArray.count > listAlertInDatabase.count {
            
            for alert in alertArray{
                
                var isAlertInDatabase = false
                
                for alertInDatabase in listAlertInDatabase{
                    
                    if(alertInDatabase.value(forKey: "id_alert") as! String == alert.idAlert!){
                        isAlertInDatabase = true
                    }
                    
                }
                
                if !isAlertInDatabase{
                    alert.groups.append(group)
                    addAlertToDatabase(alert)
                }
                
            }
            
        }else if  alertArray.count < listAlertInDatabase.count{
            
            for alert in listAlertInDatabase{
                
                var isAlertInServer = false
                
                for alertInServer in alertArray {
                    
                    if alert.value(forKey: "id_alert") as? String == alertInServer.idAlert{
                        isAlertInServer = true
                    }
                }
                
                if !isAlertInServer {
                    
                    deleteAlertInDatabase(alert: alert)
                    
                    
                }
                
            }
            
            
        }
        
        saveChangesInDatabaseUpdate(getAlertInDatabase(group),alertArray)
        
        
    }
    
    class func saveChangesInDatabaseUpdate(_ alertInDB:[AlertModel],_ alertInServer:[AlertModel]) -> Void{
        
        for alert in alertInDB{
            
            if let alertAux = getAlertInServerById(alert.value(forKey: "id_alert") as! String,alertInServer)
            {
                editAlertInDatabase(alert: alert, name: alertAux.name!, descriptionAlert: alertAux.descriptionAlert!, date: alertAux.date!, time: alertAux.time!)
            }
        }
    }
    
    class func getAlertInServerById(_ idAlert: String,_ elements: [AlertModel] ) -> AlertModel?{
        
        for alert in elements{
            
            if alert.idAlert == idAlert{
                return alert
            }
            
        }
        
        return nil
        
    }
    
    class func deleteResponseFromServer(_ dataResponse: DefaultDataResponse) -> Bool{
        
        guard dataResponse.error == nil else{
            
            return false
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return false
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return false
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return false
            
        }
        
        return true
        
    }
    
    class func updateResponseFromServer(_ dataResponse: DefaultDataResponse) -> Bool{
        
        guard dataResponse.error == nil else{
            
            return false
            
        }
        
        guard let dataResponse = dataResponse.data else{
            
            return false
            
        }
        
        let jsonResponse = try? JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments) as! [String:Any]
        
        guard (jsonResponse) != nil else{
            
            return false
            
        }
        
        if (jsonResponse!["error"] as? Int) == 1 {
            
            return false
            
        }
        
        return true
        
    }
    
}
