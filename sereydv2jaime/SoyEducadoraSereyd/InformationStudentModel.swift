//
//  InformationStudentModel.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 22/08/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import Foundation
import RealmSwift

class InformationStudentModel: Object{
    
    dynamic var attendanceAccount: Int = 0
    var descriptionBehavior : String?
    dynamic var averageBehavior: Double = 0
    dynamic var language: Double = 0
    dynamic var math: Double = 0
    dynamic var world: Double = 0
    dynamic var health: Double = 0
    dynamic var social: Double = 0
    dynamic var art: Double = 0
    dynamic var diagnostic: Double = 0
    dynamic var schoolPerformance: Double = 0
    
    dynamic var student: StudentModel?
    
}
