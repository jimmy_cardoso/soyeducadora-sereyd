//
//  HomeWorkDetailViewController.swift
//  SoyEducadoraSereyd
//
//  Created by Isaias Carrera Ventura on 02/06/17.
//  Copyright © 2017 FullStack. All rights reserved.
//

import UIKit
import RealmSwift
import Floaty
import ActionButton
import Alamofire
import NVActivityIndicatorView

class HomeWorkDetailViewController: UIViewController, UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable {

	var group: GroupModel?
	@IBOutlet weak var tableView: UITableView!
	var actionButton: ActionButton!
	var homeworks: [HomeWorkModel] = [HomeWorkModel]()
	
    override func viewDidLoad() {
        super.viewDidLoad()
		setupView()
	}
	 
	override func viewDidAppear(_ animated: Bool) {
		
		requestForHomework()
		
	}
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
    }
	
	func requestForHomework() -> Void{
		
		//GET IMAGE AND SAVE OBJECT
		self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
		
		Alamofire.request("\(Constants.GROUP_ENDPOINT)\(String(describing: group!.value(forKey: "idGroup")!))", method: .get)
		.validate()
		.response { (dataResponse) in
			
			self.stopAnimating()
			
			if let homeworks = HomeWorkController.getHomeworkListFromResponse(dataResponse) {
				print(homeworks)
				HomeWorkController.saveListUnrepeatedHomeworkInList(homeworks,self.group!)
				self.homeworks = HomeWorkController.getHomeworksForGroup(self.group!)!
				self.tableView.reloadData()
				
            }else{
                
                _ = SweetAlert().showAlert("¡Algo salió mal!", subTitle: "Inténtalo de nuevo más tarde", style: AlertStyle.error)
                
            }
			
			
		}
	} 
	
	func setupView() {
		
		actionButton = ActionButton(attachedToView: self.view, items: [])
		actionButton.action = { button in
			self.performSegue(withIdentifier: "crudHomeworkSegue", sender: self)
		}
		
		actionButton.setTitle("+", forState: UIControlState())
		actionButton.backgroundColor = ColorHelper.getSecondaryColor(1.0)
		
		homeworks = HomeWorkController.getHomeworksForGroup(group!)!
		
	}
	
	
	public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
		return homeworks.count
	}
	
	var didSelectHomeworkToEdit = false
	var selectedHomeworkToEdit: HomeWorkModel?
	
	public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! HomeWorkTableViewCell
		
		cell.nameHomeWorkLabel.text = homeworks[indexPath.row].value(forKey: "name") as? String
		cell.dateHomeworkLabel.text = homeworks[indexPath.row].value(forKey: "date") as? String
		cell.descriptionHomeworkTextView.text = homeworks[indexPath.row].value(forKey: "descriptionHomeWork") as? String
		cell.tapDeleteAction = { (cell) in
			
			_ = SweetAlert().showAlert("¿Eliminar esta tarea?", subTitle: "", style: AlertStyle.warning, buttonTitle:"Cancelar", buttonColor:ColorHelper.getGreenColor(1.0), otherButtonTitle:  "Eliminar",otherButtonColor: ColorHelper.getRedColor(1.0)) { (isOtherButton) -> Void in
				if isOtherButton != true {
					
					self.startAnimating(Constants.getSizeForLoadingView(view: self.view), type: NVActivityIndicatorType.ballBeat, color: ColorHelper.getSecondaryColor(1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
					
					let url = "\(Constants.HOMEWORK_ENDPOINT)\(String(describing: self.homeworks[indexPath.row].value(forKey: "idHomework")!))"
					
					Alamofire.request(url, method: .delete)
						.validate()
						.response(completionHandler: { (dataResponse) in
							
							self.stopAnimating()
							
							let boolResponse = HomeWorkController.getResponseFromDeleteHomework(dataResponse)
							if boolResponse{
								
								HomeWorkController.deleteHomeWorkInDatabase(homework: self.homeworks[indexPath.row])
								self.homeworks = HomeWorkController.getHomeworksForGroup(self.group!)!
								
								self.tableView.reloadData()
								_ = SweetAlert().showAlert("Tarea eliminada!", subTitle: "Has eliminado correctamente una tarea", style: AlertStyle.success)
								
							}else{
								
							}
							
						})
				}
			}
			
		}
		
		cell.tapEditAction = { (cell) in
			
			self.selectedHomeworkToEdit = self.homeworks[indexPath.row]
			self.didSelectHomeworkToEdit = true
			self.performSegue(withIdentifier: "crudHomeworkSegue", sender: self)
		
		}

		return cell
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		if segue.identifier == "crudHomeworkSegue" && self.didSelectHomeworkToEdit == true {
		
			let homeworkViewController = segue.destination as! HomeWorkCrudViewController
			homeworkViewController.group = self.group
			homeworkViewController.homework = self.selectedHomeworkToEdit
			
		}else{
			
			let homeworkViewController = segue.destination as! HomeWorkCrudViewController
			homeworkViewController.group = self.group

		}
		
	}
	
	
	
}
